# overlap\_lib
This is the library I use to perform different operations in 3D space for the
paper 

"Training Deep Neural Networks to Detect Repeatable 2D Features Using
Large Amounts of 3D World Capture Data" 
[arxiv](https://arxiv.org/abs/1912.04384)

## Features
 - Confidence painting
 - Pairwise cloud overlap detection
 - Histogram based detection repeatability test
 - Interpolating point cloud projection (C++ only)

## Installation
The installation of this library depends on the environment used. Each of the
python modules that uses this library has a Dockerfile that contains step by
step instructions that go through setting up the full environment for 
installation.

The source of each dependency matters when installing *any* TorchScript library.
The important bit is to make sure that you can load any library you use in
python. 

1. Installing Octomap
Install `liboctomap` using your system package manager. Before we can load
`overlap_lib`, we first have to load the octomap library in `__init__.py`. This
path should be checked. By default, it is `/usr/local/lib/liboctomap.so.1.9`.
You can check what your path should be using `ldconfig -p | grep octomap`.

2. Installing opencv
Install `opencv` using your system package manager. You need to install it using
your python package manager as well. I am not quite sure how the `opencv` 
python packages import the underlying libraries, but it seems to use the
default system `opencv` library. Modify the `CMakeLists.txt` so the required
`opencv` version corresponds to your system `opencv`. Change the line 
```
find_package(OpenCV 3 REQUIRED core imgcodecs imgproc features2d highgui)
```
such that the 3 corresponds to the major version found by 
`ldconfig -p | grep opencv`.  

3. Installing Open3D
We need to build Open3D from source so we can install the dynamic library so we
can import it into python.  
Oh but they broke compatibility by renaming the include folder so you might need to
change the install script path as well as simlink /usr/include/open3d to /usr/include/Open3D
```
git clone --recursive https://github.com/intel-isl/Open3D
bash util/scripts/install-deps-<your os>.sh
mkdir build
cd build
cmake -DBUILD_PYBIND11=OFF -DCMAKE_INSTALL_PREFIX=/usr/ -DBUILD_SHARED_LIBS=ON .. \
make -j4
sudo make install
```

4. Building the Library
Then, install `pytorch`. You can use any package manager for this, but you need
to know what the location of the `site-packages` corresponding to this
installation. If you are using `conda`, make sure to activate your environment
before running `cmake`.
```
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=<PATH TO site-packages>/site-packages/torch/share/cmake/Torch ..
make -j4
```
You can now test the library by opening python at the parent directory of the
`overlap_lib` and `import overlap_lib`. If you get errors, go to the 
troubleshooting section.

## Modules
`detectors` wraps different detectors to provide a uniform interface to test them.  

## Troubleshooting
If you experience missing symbols when importing the library, first take a look
at the garbled symbol, as it typically has the name of the library that python
didn't import. 

| Missing Symbol | Solution                                                                                                                    |
|----------------|-----------------------------------------------------------------------------------------------------------------------------|
| octomap        | Check your path to octomap in `__init__.py`                                                                                 |
| torch          | Make sure you selected the right `site-packages` for the build. If you changed the code, make sure your tensor types exist. |
| cv             | Change the opencv version in the CMakeLists                                                                                 |

## Fun Facts
 - You will get a linker error if you try to use `tensor.item<ushort>()`  
 - Never use linear interp on a depth map. Use nearest neighbor interp.
 - You cannot have a torchscript function with a reference to a tensor  
