import torch
import os
from collections import namedtuple
import ctypes
import cv2

#  octomap_path = "/usr/local/lib/liboctomap.so.1.9"
#  octo = ctypes.CDLL(octomap_path, mode=ctypes.RTLD_GLOBAL)
octomap_path = "/usr/local/lib/liboctree.so.0.0.1"
octo = ctypes.CDLL(octomap_path, mode=ctypes.RTLD_GLOBAL)

__path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "build/liboverlap_lib.so"))

torch.ops.load_library(__path)


# This is the confidence painting function
# Args:
#  [torch::Tensor::float32]: score_maps to reproject
#  [string]: path to pose of each score map
#  [string]: path to depth image of each score map (uint16 image)
#  string: path to binary color octomap of the map
# Returns:
#  A list of torch tensors, one for each scoremap
reproject_score_maps = torch.ops.overlap_lib.reproject_score_maps

# Args:
#  string depth_path_query
#  string depth_path_cand
#  string pose_path_query
#  string pose_path_cand
#  string calib_path
#  string dataset_name
# Returns:
#  A floating point matrix that contains the backprojection from query into
#  Candidate
compute_valid_mask = torch.ops.overlap_lib.compute_valid_mask

# Args:
#  [torch::Tensor::int32] detection_maps
#  torch::Tensor::int32 - map that is 1 if we are to evaluate the pair else 0
#  [string] pose_paths
#  [string] depth_paths
#  int64 max_dist
#  string type
#  string calib_path
# Returns:
#  A histogram of the distance between query and candidate
#  with [int64]*max_dist bins
detection_accuracy_histogram = torch.ops.overlap_lib.detection_accuracy_histogram

# Args
#  std::vector<torch::Tensors> depth_tensors,
#  std::vector<std::string> pose_paths,
#  std::string type,
#  std::string calib_path)
# Outputs: a matrix with pair overlaps
compute_valid_masks = torch.ops.overlap_lib.compute_valid_masks


#  int64_t
#  generate_octomap(
#      const std::vector<torch::Tensor> depth_maps,
#      const std::vector<std::string> & pose_paths,
#      const std::string & octomap_path,
#      const std::vector<double> & calib)
generate_octomap = torch.ops.overlap_lib.generate_octomap
