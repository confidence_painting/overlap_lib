#pragma once
#include <torch/script.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>

std::pair<torch::Tensor, torch::Tensor>
iqr(const torch::Tensor values);

torch::Tensor
biggest_blob_detection(
    const cv::Mat & backproj_bbx,
    const size_t scaling_iterations);

torch::Tensor
watershed_multi_blob_detection(
    const cv::Mat & backproj_bbx);

torch::Tensor
kmeans_multi_blob_detection(
    const cv::Mat & backproj_bbx);

torch::Tensor
iqr_blob_detection(cv::Mat & backproj_bbx_m);

torch::Tensor
min_blob_detection(cv::Mat & backproj_bbx_m);

void
draw_blobs(
    cv::Mat & canvas,
    const std::vector<torch::Tensor> blobs);
