#pragma once
#include <torch/script.h>
#include <string>
#include <vector>
#include <Eigen/Geometry>

double
calculate_full_iou(
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type);

std::vector<double>
calculate_match_iou(
    torch::Tensor pred_boxes_query,
    torch::Tensor pred_boxes_cand,
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type);

std::vector<double>
mean_max_iou(
    torch::Tensor pred_boxes_query,
    torch::Tensor pred_boxes_cand,
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type);

torch::Tensor
compute_last_targets(
    torch::Tensor target_boxes,
    torch::Tensor target_features,
    const cv::Mat & cand_depth,
    const cv::Mat & target_depth,
    const Eigen::Matrix4f & cand_pose,
    const Eigen::Matrix4f & target_pose);

std::vector<torch::Tensor>
multi_compute_last_targets(
    // Target boxes (use fewer to reduce load)
    std::vector<torch::Tensor> pred_boxes_targets,
    std::vector<torch::Tensor> features_targets,
    const std::vector<std::string> & pose_paths,
    const std::vector<std::string> & depth_paths);
