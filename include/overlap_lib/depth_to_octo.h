#pragma once

#include <torch/script.h>
#include <string>
#include <vector>

int64_t
generate_octomap(
    const std::vector<torch::Tensor> depth_maps,
    const std::vector<torch::Tensor> pose_tensors,
    const std::string & outpath,
    const std::vector<double> & calib,
    const double & res);
