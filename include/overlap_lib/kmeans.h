#pragma once
#include <vector>
#include <torch/torch.h>
#include "overlap_lib/projection.h"

typedef torch::Tensor Point;

using DataFrame = std::vector<Point>;

double squared_l2_distance(Point first, Point second);

std::tuple<DataFrame, DataFrame, float>
k_means(const DataFrame& data,
        size_t k,
        size_t number_of_iterations);
