#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <torch/script.h>

#include "overlap_lib/util.h"

#include "overlap_lib/train_util.h"
#include "overlap_lib/depth_to_octo.h"
#include "overlap_lib/test_util.h"
#include "overlap_lib/deprecated.h"

torch::Tensor
compute_valid_mask(
    const std::string & depth_path_query,
    const std::string & depth_path_cand,
    const std::string & pose_path_query,
    const std::string & pose_path_cand,
    const std::string & calib_path,
    const std::string & type);

torch::Tensor
compute_valid_masks(
    std::vector<torch::Tensor> depth_tensors,
    const std::vector<torch::Tensor> pose_tensors,
    const std::vector<double> & calib);
