#pragma once
#include <opencv2/core/core.hpp>
#include <Eigen/Geometry>
#include <Open3D/Open3D.h>

// #include <octomap/octomap.h>
// #include <octomap/OcTree.h>
// #include <octomap/ColorOcTree.h>

#include "overlap_lib/types.h"

extern cv::Mat KERNEL7;
extern cv::Mat KERNEL5;
extern cv::Mat KERNEL3;

void
back_project_score_map(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const Eigen::Matrix4d & query_pose,
    const Eigen::Matrix4d & cand_pose,
    const std::vector<double> & query_calib,
    const std::vector<double> & cand_calib,
    const cv::Mat & cand_confidence_mask,
    const float depth_factor,
    const bool interp_cloud,
    cv::Mat & backproj_map,
    cv::Mat & backproj_depth);

float
bbx_overlap(
    const cv::Mat & backprojected,
    const BoundingBox & bb);

// project 2 into 1
float
back_project(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const Eigen::Matrix4d & query_pose,
    const Eigen::Matrix4d & cand_pose,
    const std::vector<double> & query_calib,
    const std::vector<double> & cand_calib,
    const BoundingBox & query_bbx,
    const BoundingBox & cand_bbx,
    const float depth_factor,
    cv::Mat & backproj);

open3d::geometry::PointCloud 
depthToCloud(
    const cv::Mat & depth,
    const cv::Mat * color,
    const std::vector<double> & calib,
    const float factor,
    const BoundingBox & bb,
    const Eigen::Matrix4d & transform,
    const bool interp_cloud=true,
    const int decimation_factor=1,// use 1/decimation_factor^2 of the cloud
    const float pixels_per_meter=70); // Best setting for raw back proj

void
colorCloudToMat(
    cv::Mat & depth_dst,
    cv::Mat & color_dst,
    const open3d::geometry::PointCloud & cloud,
    const std::vector<double> & calib,
    const int rows, 
    const int cols, 
    const float factor);

void
colorCloudToMatHandleOcclusion(
    cv::Mat & data_counts,
    cv::Mat & depth_dst,
    cv::Mat & color_dst,
    const cv::Mat & query_depth,
    const open3d::geometry::PointCloud & cloud,
    const std::vector<double> & calib,
    const Eigen::Matrix4d trans,
    const int rows, 
    const int cols, 
    const float factor);

void
denoise_depth(const cv::Mat & depth, cv::Mat & out);
