#pragma once
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <torch/script.h>

void
gen_filter(
    const int & radius,
    const float & bin_offset,
    cv::Mat & filt);

void
nearest_neighbor_search(
    const cv::Mat & query,
    const cv::Mat & cand,
    const std::vector<cv::Mat> & dist_filters,
    cv::Mat & distances);

torch::Tensor
detection_accuracy_histogram(
    std::vector<torch::Tensor> detection_maps,
    const torch::Tensor & pair_map,
    const std::vector<std::string> & depth_paths,
    const std::vector<std::string> & pose_paths,
    const int64_t max_dist,
    const std::string & type,
    const std::string & calib_path);
