#pragma once

#include <torch/script.h>
#include <string>
#include <vector>
#include <glm/glm.hpp>

glm::vec3
calcRayPos(
    float i, float j,
    Eigen::Matrix4f & pose,
    std::vector<double> & calib);

std::vector<torch::Tensor>
reproject_score_maps(
    std::vector<torch::Tensor> score_maps,
    const std::vector<torch::Tensor> pose_tensors,
    const std::string & octomap_path,
    const std::vector<double> & calib);
