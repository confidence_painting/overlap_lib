#pragma once
#include <iostream>
#include <cstdio>
#include <cmath>

// Enable prints everywhere to find segfaults
#define PARALLEL true
#define HAIL_MARY_MODE false
#define HAIL() if (HAIL_MARY_MODE) {printf("%s:%d\n", __FILE__, __LINE__);}
#define HPRINT(x) if (HAIL_MARY_MODE) {printf("%s:%d: ", __FILE__, __LINE__); std::cout << x << std::endl << std::flush;}
#define HAIL_ONLY(x) if (HAIL_MARY_MODE) {x;}

// Enable output
#define DEBUG_MODE false
#define DEBUG() if (DEBUG_MODE) {printf("%s:%d\n", __FILE__, __LINE__);}
#define DPRINT(x) if (DEBUG_MODE) {printf("%s:%d: ", __FILE__, __LINE__); std::cout << x << std::endl << std::flush;}
#define DEBUG_ONLY(x) if (DEBUG_MODE) {x;}

struct Calibration
{
  Calibration( float fx, float fy, float cx, float cy ) : fx(fx), fy(fy), cx(cx), cy(cy) {}
  Calibration() : fx(0), fy(0), cx(0), cy(0) {}
  float fx;
  float fy;
  float cx;
  float cy;
};

const float FOV = 1.57;
const Calibration CALIB_REF ( FOV/M_PI, FOV/M_PI, 0.5, 0.5);
const float EPS = 1e-10;

// For depth calculations
const float DEPTH_FACTOR = 512;

struct BoundingBox
{
  BoundingBox( int x, int y, int w, int h ) : x(x), y(y), w(w), h(h) {}
  BoundingBox() : x(0), y(0), w(0), h(0) {}
  // Clip to be within image
  void clipWithin(int x, int y, int w, int h);
  int volume();
  int x, y, w, h;
};

std::ostream& operator<<(std::ostream& os, const Calibration c);
std::ostream& operator<<(std::ostream& os, const BoundingBox c);
