#pragma once

#include <Eigen/Geometry>

#include <opencv2/core/core.hpp>
#include <string>
#include <vector>
//#include <torch/torch.h>
#include "overlap_lib/types.h"
#include <sys/stat.h>

using namespace std;

std::string type2str(int type);

Eigen::Matrix4f
loadGibsonPose(const std::string & poseFilePath);

Eigen::Matrix4f
loadUnrealCVPose(const std::string & poseFilePath);

Eigen::Matrix4f
loadScannetPose(const std::string & poseFilePath);

void
stat_mat(cv::Mat mat,
         const char * name = "Mat");

// BoundingBox
// convertBox(
//     torch::Tensor bbx,
//     int rows,
//     int cols);
//
// torch::Tensor
// bbx_iou(torch::Tensor boxes1,
//         torch::Tensor boxes2,
//         bool x1y1x2y2=false);

void handler(int sig);

// https://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c/19841704
// Written by PherricOxide
inline bool file_exists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}
