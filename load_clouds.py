import cv2
import open3d
import json
import numpy as np
from pyquaternion import Quaternion

def load_transform(path):
    with open(path, "r") as f:
        dat = json.load(f)
        q = Quaternion(x=dat["orientation"][0], y=dat["orientation"][1], z=dat["orientation"][2], w=dat["orientation"][3])
        corr = Quaternion(axis=np.array([1, 0, 0]), degrees=-90) * Quaternion(axis=np.array([0, 1, 0]), degrees=90)
        trans = (q*corr).transformation_matrix
        #  trans = (q).transformation_matrix
        trans[0, 3] = dat["position"][0]
        trans[1, 3] = dat["position"][1]
        trans[2, 3] = dat["position"][2]
        return trans

# col1 = cand, col2 = query
col1 = open3d.Image(cv2.cvtColor(cv2.imread("0000024.png", -1), cv2.COLOR_BGR2RGB))
col2 = open3d.Image(cv2.cvtColor(cv2.imread("0000011.png", -1), cv2.COLOR_BGR2RGB))

dep1_n = cv2.imread("0000024.tiff", -1)
dep2_n = cv2.imread("0000011.tiff", -1)
dep1 = open3d.Image(dep1_n)
dep2 = open3d.Image(dep2_n)

trans1 = load_transform("0000024.json")
trans2 = load_transform("0000011.json")

intrinsic = open3d.camera.PinholeCameraIntrinsic(width=512, height=512, fx=256, fy=256, cx=256, cy=256)
rgbd1 = open3d.geometry.create_rgbd_image_from_color_and_depth(col1, dep1, depth_scale=512.0, depth_trunc=1e6, convert_rgb_to_intensity=False)
rgbd2 = open3d.geometry.create_rgbd_image_from_color_and_depth(col2, dep2, depth_scale=512.0, depth_trunc=1e6, convert_rgb_to_intensity=False)
#  pc1 = open3d.geometry.create_point_cloud_from_rgbd_image(rgbd1, intrinsic)
#  pc2 = open3d.geometry.create_point_cloud_from_rgbd_image(rgbd2, intrinsic)
pc1 = open3d.geometry.create_point_cloud_from_depth_image(dep1, depth_scale=512.0, depth_trunc=1e6, stride=10)
pc2 = open3d.geometry.create_point_cloud_from_depth_image(dep2, depth_scale=512.0, depth_trunc=1e6, stride=10)

#  pc1.transform(trans1)
#  pc2.transform(trans2)
pc2.transform(np.linalg.inv(trans1).dot(trans2))

#  open3d.draw_geometries([dep1])
#  open3d.draw_geometries([pc1, pc2])
#  open3d.draw_geometries([pc2])
img = np.zeros((512, 512), dtype=np.float32)
points = np.asarray(pc2.points)
points[:, 0] = 256*points[:, 0]/points[:, 2] + 256 + 0.5
points[:, 1] = 256*points[:, 1]/points[:, 2] + 256 + 0.5

for point in points:
    if point[0] >= 512 or point[1] >= 512 or point[0] < 0 or point[1] < 0:
        continue
    img[int(point[1]), int(point[0])] = point[2]

#  img = (img*512).astype(np.uint16)
img = (img*512)
print(img)
print(dep1_n.astype(np.float32))
print(dep1_n-img)
print(dep1_n-img>0)
#  img = np.where(dep1_n.astype(np.float32)-img<0, 0, img)
img2 = cv2.bitwise_and(img, img, mask=(dep1_n.astype(np.float32)-img>0).astype(np.uint8))
backproj = open3d.Image(img2)
open3d.draw_geometries([backproj])
#  open3d.draw_geometries([dep1])

