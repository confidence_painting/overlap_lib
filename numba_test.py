from numba import jit, njit
import torch as th
#  import open3d

@njit
def test_grad(x):
    return x**2

print(test_grad(5))
a = th.FloatTensor(5)
a.require_grad = True
loss = (test_grad(a) - 1)
loss.backward()
print(loss)
