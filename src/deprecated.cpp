#include "overlap_lib/util.h"
#include "overlap_lib/deprecated.h"
#include "overlap_lib/projection.h"
#include "overlap_lib/json.hpp"

torch::Tensor
compute_last_targets(
    torch::Tensor target_boxes,
    torch::Tensor target_features,
    const cv::Mat & cand_depth,
    const cv::Mat & target_depth,
    const Eigen::Matrix4f & cand_pose,
    const Eigen::Matrix4f & target_pose)
{
  bool DISPLAY = false;
  // For each targetidate, find the best cand in terms of overlap
  // Then return a vector of goal descriptors for each cand and a mask that
  // represents which queries have goals

  // Our variable type
  torch::TensorOptions def_opt = target_boxes.type();
  if (target_boxes.is_cuda()) {
    def_opt.device(target_boxes.device());
  }
  torch::Device device = target_boxes.device();

  // Assumes all images are the same size
  float w_ = float(cand_depth.cols);
  float h_ = float(cand_depth.rows);
  Calibration calib (CALIB_REF.fx*w_, CALIB_REF.fy*h_, CALIB_REF.cx*w_, CALIB_REF.cy*h_);
  BoundingBox full_bbx(0, 0, cand_depth.cols, cand_depth.rows);

  long n = target_boxes.size(0);

  std::vector<torch::Tensor> targets;
  for (long i=0; i<n; i++) {
    // Convert targetidate box to image coordinates
    BoundingBox target_box = convertBox(target_boxes[i], target_depth.cols, target_depth.rows);

    // Backproject targetidate box
    cv::Mat backproj;
    HAIL();
    back_project(
        cand_depth, target_depth,
        cand_pose, target_pose, calib, calib,
        full_bbx, target_box, DEPTH_FACTOR, backproj);
    HAIL();
    if (DISPLAY) {
      cv::imshow("Raw BP", backproj);
    }

    denoise_depth(backproj, backproj);

    // // Make more smooth
    // cv::blur(backproj, backproj, cv::Size(5, 5));
    // cv::threshold(backproj, backproj, 0.000001, 1, 0);
    // // Remove specs
    // cv::Mat kernel = cv::Mat::ones(7, 7, CV_32FC1);
    // cv::morphologyEx(backproj, backproj, cv::MORPH_OPEN, kernel);
    // // Close holes
    // cv::morphologyEx(backproj, backproj, cv::MORPH_CLOSE, kernel);

    if (backproj.cols == 0 || backproj.rows == 0) continue;
    
    // Get bounding box corresponding to that target
    // torch::Tensor blob = biggest_blob_detection(backproj, 3).to(device);
    torch::Tensor blob = min_blob_detection(backproj).to(device);
    std::cout << target_features[i] << std::endl;
    torch::Tensor target = torch::cat({blob, target_features[i]}, 0);
    std::cout << target << std::endl;
    targets.push_back(blob);

  }

  return torch::stack(targets);
}

std::vector<torch::Tensor>
multi_compute_last_targets(
    // Target boxes (use fewer to reduce load)
    std::vector<torch::Tensor> pred_boxes_targets,
    std::vector<torch::Tensor> features_targets,
    const std::vector<std::string> & pose_paths,
    const std::vector<std::string> & depth_paths)
{
  signal(SIGSEGV, handler);
  /*!
   * Based on the loss for yolo v2. Calculates the descriptor loss.
   *
   * pred_boxes: (batch_size, num_pred_boxes, 4) x,y center origin boxes in percentage of image
   * features: (batch_size, num_pred_boxes, arbitrary) features corresponding to the boxes
   * pose_paths: list of paths to poses for each image in the batch
   * depth_paths: list of paths to depth images for each image in the batch
   **/

  int batch_size = pred_boxes_targets.size();

  // Our variable type
  torch::TensorOptions def_opt = pred_boxes_targets[0].type();
  if (pred_boxes_targets[0].is_cuda()) {
    def_opt.device(pred_boxes_targets[0].device());
  }
  torch::Device device = pred_boxes_targets[0].device();

  // Load images and poses
  std::vector<cv::Mat> images;
  images.reserve(batch_size);

  std::vector<Eigen::Matrix4f,Eigen::aligned_allocator<Eigen::Matrix4f> > poses;
  for (int i=0; i<batch_size; i++) {
    cv::Mat im = cv::imread(depth_paths[i], -1);
    // cv::resize(im, im, cv::Size(), 0.5, 0.5);
    images.push_back(im);
    poses.push_back(loadGibsonPose(pose_paths[i]));
  }

  torch::Tensor loss = torch::tensor(0.0f, def_opt).to(device);
  std::mutex accum_lock;
  // For each target
  std::vector<torch::Tensor> targets;

#if PARALLEL
#pragma omp parallel for
#endif
  for (int cand_index=0; cand_index<batch_size; ++cand_index) {
    std::vector<torch::Tensor> cand_targets;
    for(int target_index=0; target_index<batch_size; ++target_index) {
      if (cand_index == target_index) continue;
      // find_best_matches does the following:
      // For each candidate, find the best query in terms of overlap
      // Then return a vector of goal descriptors for each query and a mask that
      // represents which queries have goals
      // That means we need to reduce the number of candidates because otherwise it takes too long
      torch::Tensor targets_i = compute_last_targets(
          pred_boxes_targets[target_index], features_targets[target_index],
          images[cand_index], images[target_index],
          poses[cand_index], poses[target_index]);
      cand_targets.push_back(targets_i);
    }
    std::lock_guard<std::mutex> guard (accum_lock);
    targets.push_back(torch::stack(cand_targets));
  }
  return targets;
}

double
calculate_full_iou(
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type)
{
  // Assumes the candidate boxes are already matched to the query and are aligned
  // Returns the full frame overlap and matched feature overlap

  // torch::Device device = pred_boxes_cand.device();

  // ============================ Begin Loading ================================

  // Load images and poses
  cv::Mat depth_query = cv::imread(depth_path_query, -1);
  cv::Mat depth_cand = cv::imread(depth_path_cand, -1);

  HAIL();
  // Calculate offset to make image square
  int img_size = min(depth_query.rows, depth_query.cols);
  cv::Rect roi (
      int((depth_query.cols - img_size)/2),
      int((depth_query.rows - img_size)/2),
      img_size, img_size
  );
  HAIL();
  depth_query = depth_query(roi);
  depth_cand = depth_cand(roi);

  Eigen::Matrix4f pose_query;
  Eigen::Matrix4f pose_cand;
  HAIL();
  if (type == "gibson") {
    pose_query = loadGibsonPose(pose_path_query);
    pose_cand = loadGibsonPose(pose_path_cand);
  } else {
    pose_query = loadScannetPose(pose_path_query);
    pose_cand = loadScannetPose(pose_path_cand);
  }
  HAIL();

  size_t img_rows = depth_query.rows;
  size_t img_cols = depth_query.cols;
  HAIL();

  Calibration calib;
  float factor = 512;
  if (type == "gibson") {
    calib = Calibration(
        CALIB_REF.fx*img_cols, CALIB_REF.fy*img_rows,
        CALIB_REF.cx*img_cols, CALIB_REF.cy*img_rows);
    factor = 512;
  } else if (type == "scannet") {
    HAIL();
    std::ifstream t(calib_path);
    if (!t.good()) {
      cout << "Failed to open calib file: " << calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    // Someone tell me why this doesn't work.
    // Calibration calib (float(js["fx"]), float(js["fy"]), float(js["cx"]), float(js["cy"]));
    calib = Calibration(fx, fy, cx - roi.x, cy - roi.y);
    factor = 1000;
  }
  HAIL();

  BoundingBox full_bbx(0, 0, depth_query.cols, depth_query.rows);

  // ============================= Done Loading ================================

  // Compare each candidate
  cv::Mat backproj;
  // Full frame overlap
  float full_frame_overlap = back_project(
      depth_query, depth_cand,
      pose_query, pose_cand,
      calib, calib,
      full_bbx, full_bbx,
      factor,
      backproj);

  return full_frame_overlap;
}

std::vector<double>
calculate_match_iou(
    torch::Tensor pred_boxes_query,
    torch::Tensor pred_boxes_cand,
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type)
{
  // Assumes the candidate boxes are already matched to the query and are aligned
  // Returns the full frame overlap and matched feature overlap

  // Our variable type
  torch::TensorOptions def_opt = pred_boxes_cand.type();
  if (pred_boxes_cand.is_cuda()) {    def_opt.device(pred_boxes_cand.device());
  }
  // torch::Device device = pred_boxes_cand.device();

  int num_matches = pred_boxes_query.size(0);

  // ============================ Begin Loading ================================

  // Load images and poses
  cv::Mat depth_query = cv::imread(depth_path_query, -1);
  cv::Mat depth_cand = cv::imread(depth_path_cand, -1);

  HAIL();
  // Calculate offset to make image square
  int img_size = min(depth_query.rows, depth_query.cols);
  cv::Rect roi (
      int((depth_query.cols - img_size)/2),
      int((depth_query.rows - img_size)/2),
      img_size, img_size
  );
  HAIL();
  depth_query = depth_query(roi);
  depth_cand = depth_cand(roi);

  Eigen::Matrix4f pose_query;
  Eigen::Matrix4f pose_cand;
  HAIL();
  if (type == "gibson") {
    pose_query = loadGibsonPose(pose_path_query);
    pose_cand = loadGibsonPose(pose_path_cand);
  } else {
    pose_query = loadScannetPose(pose_path_query);
    pose_cand = loadScannetPose(pose_path_cand);
  }
  HAIL();

  size_t img_rows = depth_query.rows;
  size_t img_cols = depth_query.cols;

  std::vector<BoundingBox> query_boxes, cand_boxes;
  for (int match_box_index=0; match_box_index<num_matches; match_box_index++) {
    query_boxes.push_back(convertBox( pred_boxes_query[match_box_index], img_rows, img_cols));
    cand_boxes.push_back(convertBox( pred_boxes_cand[match_box_index], img_rows, img_cols));
  }
  HAIL();

  Calibration calib;
  float factor = 512;
  if (type == "gibson") {
    calib = Calibration(
        CALIB_REF.fx*img_cols, CALIB_REF.fy*img_rows,
        CALIB_REF.cx*img_cols, CALIB_REF.cy*img_rows);
    factor = 512;
  } else if (type == "scannet") {
    HAIL();
    std::ifstream t(calib_path);
    if (!t.good()) {
      cout << "Failed to open calib file: " << calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    // Someone tell me why this doesn't work.
    // Calibration calib (float(js["fx"]), float(js["fy"]), float(js["cx"]), float(js["cy"]));
    calib = Calibration(fx, fy, cx - roi.x, cy - roi.y);
    factor = 1000;
  }
  HAIL();

  BoundingBox full_bbx(0, 0, depth_query.cols, depth_query.rows);

  // ============================= Done Loading ================================

  // Compare each candidate
  cv::Mat backproj;
  // Full frame overlap
  float full_frame_overlap = back_project(
      depth_query, depth_cand,
      pose_query, pose_cand,
      calib, calib,
      full_bbx, full_bbx,
      factor,
      backproj);

  float total_overlap = 0;
  HAIL();
#if PARALLEL
#pragma omp parallel for
#endif
  for (int match_box_index=0; match_box_index<num_matches; match_box_index++) {
    cv::Mat backproj_p;
    // Check first if it is present

    float overlap = back_project(
        depth_query, depth_cand,
        pose_query, pose_cand,
        calib, calib,
        query_boxes[match_box_index], cand_boxes[match_box_index],
        factor,
        backproj_p);
    total_overlap += overlap > 0.7 ? 1 : 0;
    // total_overlap += overlap;
  }
  HAIL();
  float feature_overlap = total_overlap/num_matches;

  return std::vector<double>{feature_overlap, full_frame_overlap};
}

std::vector<double>
mean_max_iou(
    torch::Tensor pred_boxes_query,
    torch::Tensor pred_boxes_cand,
    const std::string & pose_path_query,
    const std::string & depth_path_query,
    const std::string & pose_path_cand,
    const std::string & depth_path_cand,
    const std::string & calib_path,
    const std::string & type)
{
  bool DISPLAY = false;
  // Find the mean over the max overlap for each candidate
  // Our variable type
  torch::TensorOptions def_opt = pred_boxes_cand.type();
  if (pred_boxes_cand.is_cuda()) {
    def_opt.device(pred_boxes_cand.device());
  }
  // torch::Device device = pred_boxes_cand.device();

  int num_queries = pred_boxes_query.size(0);
  int num_cands = pred_boxes_cand.size(0);

  // ============================ Begin Loading ================================

  // Load images and poses
  cv::Mat depth_query = cv::imread(depth_path_query, -1);
  cv::Mat depth_cand = cv::imread(depth_path_cand, -1);
  assert(depth_query.rows == depth_cand.rows);
  assert(depth_query.cols == depth_cand.cols);

  HAIL();
  // Calculate offset to make image square
  int img_size = min(depth_query.rows, depth_query.cols);
  cv::Rect roi (
      int((depth_query.cols - img_size)/2),
      int((depth_query.rows - img_size)/2),
      img_size, img_size
  );
  HAIL();
  depth_query = depth_query(roi);
  depth_cand = depth_cand(roi);

  HAIL();
  Eigen::Matrix4f pose_query;
  Eigen::Matrix4f pose_cand;
  if (type == "gibson") {
    pose_query = loadGibsonPose(pose_path_query);
    pose_cand = loadGibsonPose(pose_path_cand);
  } else {
    pose_query = loadScannetPose(pose_path_query);
    pose_cand = loadScannetPose(pose_path_cand);
  }

  HAIL();
  size_t img_rows = depth_query.rows;
  size_t img_cols = depth_query.cols;

  std::vector<BoundingBox> query_boxes, cand_boxes;
  for (int index=0; index<num_queries; index++) {
    query_boxes.push_back(convertBox( pred_boxes_query[index], img_rows, img_cols));
  }
  for (int index=0; index<num_cands; index++) {
    cand_boxes.push_back(convertBox( pred_boxes_cand[index], img_rows, img_cols));
  }

  HAIL();
  Calibration calib;
  float factor = 0;
  if (type == "gibson") {
    calib = Calibration(CALIB_REF.fx*img_cols, CALIB_REF.fy*img_rows, CALIB_REF.cx*img_cols, CALIB_REF.cy*img_rows);
    factor = 512;
  } else if (type == "scannet") {
    std::ifstream t(calib_path);
    if (!t.good()) {
      cout << "Failed to open calib file: " << calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    // Someone tell me why this doesn't work.
    // Calibration calib (float(js["fx"]), float(js["fy"]), float(js["cx"]), float(js["cy"]));
    calib = Calibration(fx, fy, cx - roi.x, cy - roi.y);
    factor = 1000;
  }
  HAIL();
  BoundingBox full_bbx(0, 0, depth_query.cols, depth_query.rows);

  // ============================= Done Loading ================================

  float total_overlap = 0;
  HAIL();
  std::mutex accum_lock;

  int num_present = 0;
#if PARALLEL
#pragma omp parallel for
#endif
  for (int cand_index=0; cand_index<num_cands; cand_index++) {
    cv::Mat backproj;
    // First check if the candidate could have a positive detection in the query
    HAIL()
    back_project(
        depth_query, depth_cand,
        pose_query, pose_cand,
        calib, calib,
        full_bbx, cand_boxes[cand_index],
        factor,
        backproj);
    if (cv::sum(backproj)[0] <= 20) {
      continue;
    }
    HAIL()

    // Find the max overlap for each candidate
    float max_overlap = 0;
    int best_i = -1;
    for (int query_index=0; query_index<num_queries; query_index++) {
      float overlap = back_project(
          depth_query, depth_cand,
          pose_query, pose_cand,
          calib, calib,
          query_boxes[query_index], cand_boxes[cand_index],
          factor,
          backproj);
      if (overlap > max_overlap) {
        max_overlap = overlap;
        best_i = query_index;
      }
    }
    HAIL()
    std::lock_guard<std::mutex> guard (accum_lock);
    num_present++;
    total_overlap += max_overlap > 0.2 ? 1 : 0;

    if (DISPLAY) {
      cv::Mat canvas = cv::Mat::zeros(cv::Size(depth_query.cols, depth_query.rows), CV_32FC1);
      cv::Mat canvas2 = cv::Mat::zeros(cv::Size(depth_query.cols, depth_query.rows), CV_32FC1);
      draw_blobs(
          canvas,
          {pred_boxes_cand[cand_index]});
      draw_blobs(
          canvas2,
          {pred_boxes_query[best_i]});
      std::cout << max_overlap << std::endl;
      // cv::imshow(depth_path_cand, canvas);
      // cv::imshow(depth_path_query, canvas2);
      cv::waitKey(0);
    }
  }
  HAIL();
  return std::vector<double>{total_overlap, double(num_present)};
}

