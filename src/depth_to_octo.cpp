#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <torch/script.h>

#include "overlap_lib/util.h"
#include "overlap_lib/train_util.h"
#include "overlap_lib/projection.h"
#include "half.h"
#include "octree/octree.h"

int64_t
generate_octomap(
    const std::vector<torch::Tensor> depth_maps,
    const std::vector<torch::Tensor> pose_tensors,
    const std::string & outpath,
    const std::vector<double> & calib,
    const double & res)
{

  /*!
   * A variation of the SuperPoint algorithm applied to bounding boxes
   *
   * the depth_maps must be prescaled by their depth factor and type float
   * pose_paths: list of paths to poses for each image in the batch
   * calib: [fx, fy, cx, cy]
   **/

  // ----------------------------- LOADING ---------------------------------- //

  const int batch_size = depth_maps.size();
  const int im_rows = depth_maps[0].size(0);
  const int im_cols = depth_maps[0].size(1);
  const BoundingBox full_bbx(0, 0, im_cols, im_rows);

  Eigen::AngleAxisd rot (-M_PI/2, Eigen::Vector3d::UnitX());
  Eigen::Matrix4d corr = (Eigen::Translation3d(0, 0, 0) * rot).matrix();

  std::vector<cv::Mat> images;
  images.reserve(batch_size);

  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > poses;
  for (int i=0; i<batch_size; i++) {
    // Flipped to apply -90 rotation to correct for meshes not being z up
    // poses.push_back(corr*loadGibsonPose(pose_paths[i]).cast<double>());
    // poses.push_back(loadGibsonPose(pose_paths[i]).cast<double>());
    torch::Tensor t = pose_tensors[i];
    Eigen::Matrix4d pose;
    pose << t[0][0].item<double>(), t[0][1].item<double>(), t[0][2].item<double>(), t[0][3].item<double>(), 
            t[1][0].item<double>(), t[1][1].item<double>(), t[1][2].item<double>(), t[1][3].item<double>(), 
            t[2][0].item<double>(), t[2][1].item<double>(), t[2][2].item<double>(), t[2][3].item<double>(), 
            t[3][0].item<double>(), t[3][1].item<double>(), t[3][2].item<double>(), t[3][3].item<double>();
    poses.push_back(pose);
    // poses.push_back(loadUnrealCVPose(pose_paths[i]).cast<double>());
    // poses.push_back(loadScannetPose(pose_paths[i]).cast<double>());
    cv::Mat depth ( /*rows=*/depth_maps[i].size(0),
                    /*cols=*/depth_maps[i].size(1),
                    /*type=*/CV_32FC1,
                    /*data=*/depth_maps[i].data<float>());
    images.push_back(depth);
  }

  // -------------------------- DONE LOADING -------------------------------- //

  // Compile a map using all viewpoints

  // float res = 0.01;
  const bool lazy_eval = true;
  octree::Octree tree (res);

  open3d::geometry::PointCloud pc;
// #if PARALLEL
// #pragma omp parallel for schedule(guided)
// #endif
  for(int i=0; i<batch_size; i++) {
    int bar_width = 70;
    std::cout << "[";
    float progress = float(i)/batch_size;
    int pos = bar_width*progress;
    for (int i=0; i<bar_width; i++) {
      if (i<pos) std::cout << "=";
      else if (i==pos) std::cout << ">";
      else std::cout << " ";
    }
    std::cout << "] " << int(progress*100.0) << " %\r";
    std::cout.flush();

    Eigen::Matrix4d t = poses[i];
    // Do not enable interpolation
    // It just slows things down a ton and introduces errors.
    // Can't figure out why sections protrude
    open3d::geometry::PointCloud partial_pc = depthToCloud(
        images[i], NULL, calib, 1, full_bbx,
        t, /*interp_cloud*/false, /*decimation factor*/1, 100);


    // insert point cloud
    for (size_t j=0; j<partial_pc.points_.size(); j++) {
      Eigen::Vector3d & pt = partial_pc.points_[j];
      tree.insert({float(pt[0]), float(pt[1]), float(pt[2])}, 0);
    }
  }

  tree.save(outpath, 2e9);
  return 0;
}

