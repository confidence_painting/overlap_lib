// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <cmath>
#include <queue>
#include "octree/octree.h"
#include <iostream>
#include <fstream>

namespace octree {

OctreeNode emptyNode() {
  return OctreeNode{.children={NULL_VOX,NULL_VOX,NULL_VOX,NULL_VOX,
                               NULL_VOX,NULL_VOX,NULL_VOX,NULL_VOX}};
}

void vec3print(vec3 a) {
  printf("(%f, %f, %f)", a.x, a.y, a.z);
}

int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score)
{
  // Returns the new head index

  FLinkedList item = {score, desired_address, -1};

  int new_head_index = head_index;
  int selection = head_index;
  int parent = head_index;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    list[0] = item;
    return 0;
  } else {
    while (selection != -1 && list[selection].score < item.score) {
      parent = selection;
      selection = list[selection].child;
    }
    if (selection == parent) {
      if (list[selection].score > item.score) {
        item.child = head_index;
        new_head_index = item.address;
      } else {
        list[parent].child = desired_address;
      }
    } else {
      item.child = selection;
      list[parent].child = desired_address;
    }
  }
  list[item.address] = item;
  return new_head_index;
}

bool
rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, float * ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  vec3 dirfrac = {1/rdir.x, 1/rdir.y, 1/rdir.z};
  // rpos is origin of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      *ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      *ray_length = tmax;
      return false;
  }

  *ray_length = tmin;
  return true;
}

Octree::Octree(float scale, vec3 origin) : scale(scale), origin(origin), current_max_depth(0) {
  data.push_back(emptyNode());
}

Octree::Octree(std::string fname, size_t max_chunk_size) {
  // This is just the sample code for reading and writing
  std::ifstream is(fname, std::ifstream::binary);
  if (is) {
    // get length of file:
    is.seekg (0, is.end);
    size_t length = is.tellg();
    is.seekg (0, is.beg);

    // char * buffer = new char [length];
    printf("Reading %zu characters...\n", length);
    // read data in chunks

    int64_t * buffer = (int64_t *)malloc(max_chunk_size);
    is.read ((char *)buffer, HEADER_SIZE);
    origin.x = *reinterpret_cast<float*>(&buffer[0]);
    origin.y = *reinterpret_cast<float*>(&buffer[1]);
    origin.z = *reinterpret_cast<float*>(&buffer[2]);
    scale = *reinterpret_cast<float*>(&buffer[3]);
    current_max_depth = static_cast<int>(buffer[4]);

    is.seekg(HEADER_SIZE);
    for (size_t i=HEADER_SIZE; i<length; i+=max_chunk_size) {
      is.read ((char *)buffer, max_chunk_size);
      is.seekg(i+max_chunk_size);
      size_t data_left = std::min(length-i, max_chunk_size);
      for (size_t j=0; j<data_left/NUM_DATA_PER_NODE/sizeof(int64_t); j++) {
        OctreeNode n;
        for (int k=0; k<NUM_DATA_PER_NODE; k++) {
          n.children[k] = buffer[j*NUM_DATA_PER_NODE+k];
        }
        data.push_back(n);
      }
    }

    delete[] buffer;
  } else {
    std::cout << "File " << fname << " was not found." << std::endl;
  }
}

int 
Octree::save(std::string fname, size_t max_chunk_size) {
  std::cout << "Saving tree with size: " << getBinarySize() << std::endl;
  // char * buffer = new char[getBinarySize()];
  // binarize(buffer);
  std::ofstream outfile (fname, std::ofstream::binary);

  // Write Header
  size_t buf_size = max_chunk_size/sizeof(int64_t);
  int64_t * buffer = (int64_t *)malloc(max_chunk_size);
  buffer[0] = *reinterpret_cast<int64_t*>(&origin.x);
  buffer[1] = *reinterpret_cast<int64_t*>(&origin.y);
  buffer[2] = *reinterpret_cast<int64_t*>(&origin.z);
  buffer[3] = *reinterpret_cast<int64_t*>(&scale);
  buffer[4] = static_cast<int64_t>(current_max_depth);
  outfile.write ((char *)buffer, HEADER_SIZE);

  // Write body
  for (size_t i=0; i<data.size(); i++) {
    size_t start_ind = i*8;
    for (size_t j=0; j<8; j++) {
      size_t remainder = (start_ind+j)%buf_size;
      if ((remainder == 0) && !(i==0 && j==0)) {
        // printf("Writing: %zu bytes\n", max_chunk_size);
        outfile.write ((char *)buffer, max_chunk_size);
      }
      buffer[remainder] = data[i].children[j];
    }
  }
  size_t extra = (data.size()*NUM_DATA_PER_NODE*sizeof(int64_t)) % max_chunk_size;

  // printf("Writing: %zu bytes\n", extra);
  if(extra != 0) {
    outfile.write ((char *)buffer, extra);
  }

  outfile.close();
  delete [] buffer;
  return 0;
}

// pure
bool
Octree::withinBounds(vec3 pt)
{
  float min_x = origin.x - getRadius();
  float max_x = origin.x + getRadius();

  float min_y = origin.y - getRadius();
  float max_y = origin.y + getRadius();

  float min_z = origin.z - getRadius();
  float max_z = origin.z + getRadius();
  return (min_x < pt.x && max_x > pt.x) &&
         (min_y < pt.y && max_y > pt.y) &&
         (min_z < pt.z && max_z > pt.z);
}

// pure
float
Octree::getRadius() {
  return float(std::pow(2, current_max_depth)) * scale;
}

float
Octree::voxelCoord(float v, int depth) {
  // assumes 0 origin
  float res = scale*pow(2, current_max_depth-depth+1);
  float voxelCenter = (floorf(v/res)+0.5)*res;
  if (depth == 0) {
    return v/res;
  }
  return (v - voxelCenter)/res;
}

// pure
int
Octree::childIndex(vec3 pt, int depth) {
  // Function assumes point is within bounds
  // First, get location within the parent voxel
  float vx = voxelCoord(pt.x-origin.x, depth);
  float vy = voxelCoord(pt.y-origin.y, depth);
  float vz = voxelCoord(pt.z-origin.z, depth);

  // This is the location with in the voxel
  // This bit works for sure
  for (int i=0; i<8; i++) {
    const vec3 & bounds = VOXEL_NUMBERING[i];
    if ((vx*bounds[0] <= 1 && vx*bounds[0] >= 0) &&
        (vy*bounds[1] <= 1 && vy*bounds[1] >= 0) &&
        (vz*bounds[2] <= 1 && vz*bounds[2] >= 0))
    {
      return i;
    }
  }
  return 0;
}

void
Octree::incrementDepth() {
  OctreeNode old_tree = *getNode(0);
  *getNode(0) = emptyNode();
  current_max_depth++;
  // Iterated through each child and assign it to the inner child position
  for (int i=0; i<8; i++) {
    vec3 pt = VOXEL_NUMBERING[i] * getRadius()/4.0f + origin;
    int j = childIndex(pt, 1);
    OctreeNode new_child = emptyNode();
    new_child.children[j] = old_tree.children[i];
    int ind = data.size();
    data.push_back(new_child);
    getNode(0)->children[i] = ind;
  }
}

void 
Octree::insert(vec3 pt, int64_t val) {
  if (!withinBounds(pt)) {
    // Reroot tree

    float max_diff = std::max( std::max(
          std::abs(pt.x - origin.x),
          std::abs(pt.y - origin.y)),
        std::abs(pt.z - origin.z));
    float diff = int(ceil(max_diff/getRadius()));
    // int required_depth = ceil(log(max_diff/scale)/log(2));
    int iterations = log(diff)/log(2)+1;
    // if (iterations != required_depth-current_max_depth) {
    //   printf("Required Depth: %i\n", required_depth);
    //   printf("Current Depth: %i\n", current_max_depth);
    //   printf("Iterations: %i\n", iterations);
    // }
    for (int i=0; i<iterations; i++) {
      incrementDepth();
    }
  }
  // Traverse tree, creating nodes
  // Get leaf
  int64_t current_branch = 0;
  for (int i=0; i < current_max_depth; i++) {
    int j = childIndex(pt, i);
    if (getNode(current_branch)->children[j] == NULL_VOX) {
      getNode(current_branch)->children[j] = data.size();
      data.push_back(emptyNode());
    }
    current_branch = getNode(current_branch)->children[j];
  }
  // set data
  int val_j = childIndex(pt, current_max_depth);
  getNode(current_branch)->children[val_j] = val;
}

int64_t *
Octree::get(vec3 pt) {
  if (!withinBounds(pt)) {
    return NULL;
  }
  int64_t current_branch = 0;
  for (int i=0; i < current_max_depth; i++) {
    int j = childIndex(pt, i);

    if (getNode(current_branch)->children[j] == NULL_VOX) {
      return NULL;
    }
    current_branch = getNode(current_branch)->children[j];
  }
  // set data
  int val_j = childIndex(pt, current_max_depth);
  if (getNode(current_branch)->children[val_j] == NULL_VOX) {
    return NULL;
  }
  return &getNode(current_branch)->children[val_j];
}

int64_t *
Octree::raycast(const vec3 & source,
                const vec3 & dir,
                float * dist)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with origin and depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  vec3 ray = glm::normalize(dir);

  // initialize stack
  int max_len = int(pow(2, current_max_depth));
  vec3 voxel_centers[max_len];
  int depths[max_len];
  // Initialize the depths so we know when we run out
  for (int i=0; i<max_len; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[max_len];

  int stack_head = 0;
  voxel_centers[stack_head] = origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;

  while (depths[stack_head] != -1) {
    vec3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int depth = depths[stack_head];

    int head = -1;
    float s = scale*pow(2, current_max_depth-depth);
    FLinkedList list[8];
    vec3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNode(voxel_ind)->children[i] == NULL_VOX) {
        continue;
      }
      auto & bound = VOXEL_NUMBERING[i];

      centers[i] = voxel_origin + s*bound/2.0f;
      vec3 vmin = centers[i] - s/2.0f-EPS;
      vec3 vmax = centers[i] + s/2.0f+EPS;

      float ray_length;
      bool collision = rayBoxIntersect(source, ray, vmin, vmax, &ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(list, head, i, -ray_length);
    }
    int selection = head;

    // Iterate through the sorted voxels
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      float d = -list[selection].score;
      selection = list[selection].child;
      int64_t & new_ind = getNode(voxel_ind)->children[j];

      // Check if voxel is empty
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum depth, return
      if (depth == current_max_depth) {
        *dist = d;
        return &new_ind;
      }
      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = depth+1;
      stack_head = (stack_head-1 + max_len) % max_len;
    }
    stack_head = (stack_head+1)%max_len;
  }
  return NULL;
}

size_t
Octree::getBinarySize() {
  return (NUM_EXTRA_FIELDS+NUM_DATA_PER_NODE*data.size())*sizeof(int64_t)/sizeof(char);
}

void
Octree::binarize(char * buffer) {
  // assumes buffer is allocated
  int64_t *int_buffer = (int64_t *) buffer;
  // layout of data:
  // Origin x
  // Origin y
  // Origin z
  // Scale
  // current_max_depth
  // data
  int_buffer[0] = *reinterpret_cast<int64_t*>(&origin.x);
  int_buffer[1] = *reinterpret_cast<int64_t*>(&origin.y);
  int_buffer[2] = *reinterpret_cast<int64_t*>(&origin.z);
  int_buffer[3] = *reinterpret_cast<int64_t*>(&scale);
  int_buffer[4] = static_cast<int64_t>(current_max_depth);
  for (size_t i=0; i<data.size(); i++) {
    size_t start_ind = i*8+NUM_EXTRA_FIELDS;
    for (size_t j=0; j<8; j++) {
      int_buffer[start_ind+j] = data[i].children[j];
    }
  }
}

int
Octree::initFromBinary(char * buffer, size_t length) {
  size_t data_ratio = sizeof(int64_t)/sizeof(char);
  if (length % data_ratio != 0) {
    return -1;
  }
  int64_t *int_buffer = (int64_t *) buffer;

  origin.x = *reinterpret_cast<float*>(&int_buffer[0]);
  origin.y = *reinterpret_cast<float*>(&int_buffer[1]);
  origin.z = *reinterpret_cast<float*>(&int_buffer[2]);
  scale = *reinterpret_cast<float*>(&int_buffer[3]);
  current_max_depth = static_cast<int>(int_buffer[4]);
  size_t num_nodes = (length/data_ratio-NUM_EXTRA_FIELDS)/NUM_DATA_PER_NODE;
  data.resize(num_nodes);
  for (size_t i=0; i<num_nodes; i++) {
    OctreeNode n;
    for (size_t j=0; j<8; j++) {
      n.children[j] = int_buffer[NUM_EXTRA_FIELDS+i*8+j];
    }
    data[i] = n;
  }
  return 0;
}
};
