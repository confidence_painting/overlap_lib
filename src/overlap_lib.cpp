#include <iostream>
#include <list>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
//#include <opencv2/cudafeatures2d.hpp>

#include <torch/script.h>

#include <Eigen/StdVector>

#include "overlap_lib/util.h"
#include "overlap_lib/overlap_lib.h"
#include "overlap_lib/blob_detection.h"
#include "overlap_lib/projection.h"
#include "overlap_lib/json.hpp"

using json = nlohmann::json;

torch::Tensor
compute_valid_masks(
    std::vector<torch::Tensor> depth_tensors,
    const std::vector<torch::Tensor> pose_tensors,
    const std::vector<double> & calib)
{
  // Depth map is of type int32 because there is no uint16 in torch
  //============================================================================
  //                           Same Loading Code
  //============================================================================
  assert(depth_tensors.size() == pose_tensors.size());

  HAIL();
  // Load pose and depth
  const int batch_size = pose_tensors.size();
  const int im_rows = depth_tensors[0].size(0);
  const int im_cols = depth_tensors[0].size(1);
  HPRINT(im_rows);
  HPRINT(im_cols);
  HPRINT(batch_size);

  // Load images and poses
  std::vector<cv::Mat> depth_ims, score_maps;
  depth_ims.reserve(batch_size);

  std::cout << "Loading" << std::endl;
  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > poses;
  for (int i=0; i<batch_size; i++) {

    // Convert Mat (don't worry, this doesn't copy the data)
    cv::Mat im (im_rows, im_cols, CV_32FC1,
                depth_tensors[i].data<float>());
    // im.convertTo(im, CV_16UC1, 1);
    depth_ims.push_back(im);

    torch::Tensor t = pose_tensors[i];
    Eigen::Matrix4d pose;
    pose << t[0][0].item<double>(), t[0][1].item<double>(), t[0][2].item<double>(), t[0][3].item<double>(), 
            t[1][0].item<double>(), t[1][1].item<double>(), t[1][2].item<double>(), t[1][3].item<double>(), 
            t[2][0].item<double>(), t[2][1].item<double>(), t[2][2].item<double>(), t[2][3].item<double>(), 
            t[3][0].item<double>(), t[3][1].item<double>(), t[3][2].item<double>(), t[3][3].item<double>();
    poses.push_back(pose);
  }
  HAIL();

  // Assumes all images are the same size
  const BoundingBox full_bbx(0, 0, im_cols, im_rows);

  float depth_factor = 1;

  // ============================= Done Loading ================================
  torch::Tensor gt = torch::zeros({batch_size, batch_size}, torch::kFloat);

  // Compare each candidate
  // Full frame overlap
  std::cout << "Making " << batch_size*batch_size - batch_size << " comparisons" << std::endl;
  for (int i=0; i<batch_size; i++) {
    gt[i][i] = 1;
// #if PARALLEL
#pragma omp parallel for schedule(static)
// #endif
    for (int j=0; j<batch_size; j++) {
      if (i == j) {
        continue;
      }
      float v;
      // Perform simple distance check to see if they could possibly overlap
      double max_dist1, min_dist1, max_dist2, min_dist2;
      minMaxLoc(depth_ims[i], &min_dist1, &max_dist1);
      minMaxLoc(depth_ims[j], &min_dist2, &max_dist2);

      float dist = (poses[i].inverse()*poses[j]).block<3, 1>(0, 3).norm();
      if (dist > (max_dist1 + max_dist2)/depth_factor) {
        // No possible overlap
        v = 0;
      } else {
        // Actually get overlap using point clouds
        cv::Mat backproj;
        back_project(
            depth_ims[i], depth_ims[j],
            poses[i], poses[j],
            calib, calib,
            full_bbx, full_bbx,
            depth_factor,
            backproj);
        cv::dilate(backproj, backproj, KERNEL3);
        // backproj.convertTo(backproj, CV_32FC1);
        cv::threshold(backproj, backproj, 0.1, 1, cv::THRESH_BINARY);
        // cv::imshow("Final", backproj);
        v = cv::mean(backproj)[0];
      }

#pragma omp critical (assign_gt)
      {
        // printf("%i, %i: %f\n", i, j, v);
        // if (v > 0.5) {
        //   printf("%i, %i: %f\n", i, j, v);
        // }
        gt[i][j] = v;
        // gt[j][i] = v;
      }
    }
  }

  return gt;
}

torch::Tensor
compute_valid_mask(
    const std::string & depth_path_query,
    const std::string & depth_path_cand,
    const std::string & pose_path_query,
    const std::string & pose_path_cand,
    const std::string & calib_path,
    const std::string & type)
{
  /* Args:
   *  string depth_path_query
   *  string depth_path_cand
   *  string pose_path_query
   *  string pose_path_cand
   *  string calib_path
   *  string dataset name. Either gibson or scannet
   * Output:
   *  A floating point matrix that contains the backprojection from query into
   *  candidate
   */

  // ============================ Begin Loading ================================

  // Check if files exist
  HAIL();
  assert(file_exists(depth_path_query));
  assert(file_exists(depth_path_cand));
  assert(file_exists(pose_path_query));
  assert(file_exists(pose_path_cand));

  // Load images and poses
  cv::Mat depth_query = cv::imread(depth_path_query, -1);
  cv::Mat depth_cand = cv::imread(depth_path_cand, -1);
  // cv::resize(depth_query, depth_query, cv::Size(), 0.5, 0.5);
  // cv::resize(depth_cand, depth_cand, cv::Size(), 0.5, 0.5);

  HAIL();
  // Calculate offset to make image square
  int img_size = min(depth_query.rows, depth_query.cols);
  cv::Rect roi (
      int((depth_query.cols - img_size)/2),
      int((depth_query.rows - img_size)/2),
      img_size, img_size);
  HAIL();
  /*
  depth_query = depth_query(roi);
  depth_cand = depth_cand(roi);
  */

  Eigen::Matrix4d pose_query;
  Eigen::Matrix4d pose_cand;
  HAIL();
  if (type == "gibson") {
    pose_query = loadGibsonPose(pose_path_query).cast<double>();
    pose_cand = loadGibsonPose(pose_path_cand).cast<double>();
  } else {
    pose_query = loadScannetPose(pose_path_query).cast<double>();
    pose_cand = loadScannetPose(pose_path_cand).cast<double>();
  }
  HAIL();

  size_t img_rows = depth_query.rows;
  size_t img_cols = depth_query.cols;
  HAIL();

  std::vector<double> calib;
  float depth_factor = 512;
  if (type == "gibson") {
    calib = {
        CALIB_REF.fx*img_cols, CALIB_REF.fy*img_rows,
        CALIB_REF.cx*img_cols, CALIB_REF.cy*img_rows};
    depth_factor = 512;
  } else if (type == "scannet") {
    HAIL();
    assert(file_exists(calib_path));
    std::ifstream t(calib_path);
    if (!t.good()) {
      cout << "Failed to open calib file: " << calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    // Someone tell me why this doesn't work.
    // std::vector<double> calib (float(js["fx"]), float(js["fy"]), float(js["cx"]), float(js["cy"]));
    // calib = std::vector<double>(fx, fy, cx - roi.x, cy - roi.y);
    calib = {fx, fy, cx, cy};
    depth_factor = 1000;
  }
  HAIL();

  BoundingBox full_bbx(0, 0, depth_query.cols, depth_query.rows);

  // ============================= Done Loading ================================

  // Compare each candidate
  cv::Mat backproj;
  // Full frame overlap
  // float full_frame_overlap = back_project(
  back_project(
      depth_query, depth_cand,
      pose_query, pose_cand,
      calib, calib,
      full_bbx, full_bbx,
      depth_factor,
      backproj);
  cv::dilate(backproj, backproj, KERNEL3);

  torch::Tensor backproj_t = torch::from_blob(backproj.ptr<float>(), /*sizes=*/{backproj.rows, backproj.cols}).clone();
  return backproj_t;
}


static auto registry =
  torch::RegisterOperators("overlap_lib::reproject_score_maps", &reproject_score_maps)
  .op("overlap_lib::generate_octomap", &generate_octomap)
  .op("overlap_lib::compute_valid_mask", &compute_valid_mask)
  .op("overlap_lib::compute_valid_masks", &compute_valid_masks)
  // .op("overlap_lib::calculate_match_iou", &calculate_match_iou)
  // .op("overlap_lib::mean_max_iou", &mean_max_iou)
  .op("overlap_lib::detection_accuracy_histogram", &detection_accuracy_histogram);
  // .op("overlap_lib::calculate_full_iou", &calculate_full_iou);
