#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>

#include "overlap_lib/projection.h"

cv::Mat KERNEL7 = cv::Mat::ones(7, 7, CV_32FC1);
cv::Mat KERNEL5 = cv::Mat::ones(5, 5, CV_32FC1);
cv::Mat KERNEL3 = cv::Mat::ones(3, 3, CV_32FC1);

void
back_project_score_map(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const Eigen::Matrix4d & query_pose,
    const Eigen::Matrix4d & cand_pose,
    const std::vector<double> & query_calib,
    const std::vector<double> & cand_calib,
    const cv::Mat & cand_confidence_mask,
    const float depth_factor,
    const bool interp_cloud,
    cv::Mat & backproj_map,
    cv::Mat & backproj_depth)
{
  bool DISPLAY = false;

  HAIL();
  // if (DISPLAY) {
  //   cv::imshow("mask", 255*cand_confidence_mask);
  //   cv::waitKey(0);
  // }

  // cand_depth.copyTo(masked_depth, src_mask);

  // Need to add the bounding box here
  // Check bounds when iterating
  // Only project within bounding box 2, then check output on bounding boxes2
  open3d::geometry::PointCloud pc;
  Eigen::Matrix4d t = (query_pose.inverse()*cand_pose);
  BoundingBox full_bbx (0, 0, cand_depth.cols, cand_depth.rows);
  pc = depthToCloud(cand_depth, &cand_confidence_mask, cand_calib, depth_factor, full_bbx, t, interp_cloud, /*decimation factor*/1);
  HAIL();

  cv::Mat cand_reproj, cand_reproj_col;
  colorCloudToMat(cand_reproj, cand_reproj_col, pc, cand_calib,
                  cand_depth.rows, cand_depth.cols, depth_factor);
  cv::dilate(cand_reproj, cand_reproj, KERNEL3);

  // If 1 is in front of 2, then mask out 2
  // Remove occlusion
  cv::Mat query_scaled, cand_scaled;
  query_depth.convertTo(query_scaled, CV_32FC1, 1);
  cv::Mat mask = cv::abs(query_scaled - cand_reproj) <= depth_factor*0.35; // don't forget about the depth factor

  HAIL();
  cand_reproj_col.copyTo(backproj_map, mask);
  cand_reproj.copyTo(backproj_depth, mask);

  HAIL();
  if (DISPLAY) {
    cand_depth.convertTo(cand_scaled, CV_32FC1, 1);
    cv::imshow("Diff", cv::abs(query_scaled - cand_reproj)/524);
    cv::imshow("CBj", cand_reproj/2024);
    // cv::imshow("CBjCol", cand_reproj_col);
    cv::imshow("Bj", backproj_depth/2024);
    cv::imshow("Mask", mask);
    // cv::imshow("align", cand_reproj/1500+query_scaled/1500);
    cv::imshow("query_depth", query_scaled/2024);
    cv::imshow("cand_depth", cand_scaled/2024);
    cv::waitKey(0);
  }
}

// project 2 into 1
float
back_project(
    const cv::Mat & query_depth,
    const cv::Mat & cand_depth,
    const Eigen::Matrix4d & query_pose,
    const Eigen::Matrix4d & cand_pose,
    const std::vector<double> & query_calib,
    const std::vector<double> & cand_calib,
    const BoundingBox & query_bbx,
    const BoundingBox & cand_bbx,
    const float depth_factor,
    cv::Mat & backproj)
{
  bool DISPLAY = false;
  // Generate clouds

  // Need to add the bounding box here
  // Check bounds when iterating
  // Only project within bounding box 2, then check output on bounding boxes2
  open3d::geometry::PointCloud pc;
  Eigen::Matrix4d t = query_pose.inverse()*cand_pose;
  HAIL()
  pc = depthToCloud(cand_depth, NULL, cand_calib, depth_factor, cand_bbx, t, false, 1);
  HAIL()

  cv::Mat cand_reproj, cand_reproj_col;
  colorCloudToMat(cand_reproj, cand_reproj_col, pc, cand_calib,
                  cand_depth.rows, cand_depth.cols, depth_factor);
  cv::dilate(cand_reproj, cand_reproj, KERNEL3);
  HAIL();

  // If 1 is in front of 2, then mask out 2
  // Remove occlusion
  cv::Mat query_scaled;
  query_depth.convertTo(query_scaled, CV_32FC1, 1);

  HPRINT(cand_reproj.rows);
  HPRINT(cand_reproj.cols);
  HPRINT(cand_depth.rows);
  HPRINT(cand_depth.cols);
  HPRINT(query_scaled.rows);
  HPRINT(query_scaled.cols);
  // cand_depth.convertTo(cand_scaled, CV_32FC1, 1);
  cv::Mat mask = cv::abs(query_scaled - cand_reproj) <= depth_factor*0.35;
  HAIL()

  HAIL();
  cand_reproj.copyTo(backproj, mask);

  if (DISPLAY) {
    cv::Mat disp1, disp2, disp3, disp4;
    cv::normalize(query_scaled, disp1, 255, 0, cv::NORM_MINMAX, CV_8UC1);
    cv::normalize(cand_reproj, disp2, 255, 0, cv::NORM_MINMAX, CV_8UC1);
    cv::normalize(backproj, disp3, 255, 0, cv::NORM_MINMAX, CV_8UC1);
    cv::normalize(mask, disp4, 255, 0, cv::NORM_MINMAX, CV_8UC1);
    std::cout << cv::mean(query_depth) << std::endl;
    std::cout << cv::mean(query_scaled) << std::endl;
    std::cout << cv::mean(cand_depth) << std::endl;
    std::cout << cv::mean(cand_reproj) << std::endl;
    cv::imshow("Query", disp1);
    cv::imshow("CBj", disp2);
    cv::imshow("Backproj masked", disp3);
    cv::imshow("Mask", disp4);
    cv::waitKey(0);
  }

  // Crop to bounding box in image 1
  HAIL()
  return bbx_overlap(backproj, query_bbx);
}

float
bbx_overlap(
    const cv::Mat & backprojected,
    const BoundingBox & bb)
{
  bool DISPLAY = false;
  if (bb.h == 0 || bb.w == 0) {
    return 0;
  }
  HAIL();
  cv::Mat backproj_denoised;
  denoise_depth(backprojected, backproj_denoised);

  HAIL();
  cv::Mat bbd = cv::Mat::zeros(cv::Size(bb.w, bb.h), CV_32FC1);
  backproj_denoised(cv::Rect_<int>(bb.x, bb.y, bb.w, bb.h)).copyTo(bbd);
  HAIL();
  float union_area = cv::sum(backproj_denoised)[0] + bb.w*bb.h - cv::sum(bbd)[0];
  if (DISPLAY) {
    cv::Mat canvas;
    backproj_denoised.copyTo(canvas);
    // float size = backproj_denoised.rows;
    // draw_blobs(canvas, {at::tensor({bb.x/size+bb.w/size/2, bb.y/size+bb.h/size/2, bb.w/size, bb.h/size})});
    cv::imshow("Backproj_denoised", canvas);
    // std::cout << backproj_denoised << std::endl;
    cv::imshow("Backproj", backprojected);
    // std::cout << cv::sum(bbd)[0] << std::endl;
    // std::cout << union_area << std::endl;
    // std::cout << cv::sum(backproj_denoised)[0] << std::endl;
    // std::cout << bb.w*bb.h << std::endl;
    std::cout << "IOU: " << cv::sum(bbd)[0] / union_area << std::endl;
    cv::waitKey(0);
  }

  // Take the IOU
  return cv::sum(bbd)[0] / union_area;
  // return cv::mean(bbd)[0];
}


void
colorCloudToMat(
    cv::Mat & depth_dst,
    cv::Mat & color_dst,
    const open3d::geometry::PointCloud & cloud,
    const std::vector<double> & calib,
    const int rows, 
    const int cols, 
    const float factor)
{
  // Color is monochrome for efficiency
  bool use_color = cloud.colors_.size() == cloud.points_.size();

  depth_dst = cv::Mat(rows, cols, CV_32FC1, cv::Scalar(0));
  color_dst = cv::Mat(rows, cols, CV_32FC1, cv::Scalar(0));
  HAIL();
  HPRINT(cloud.points_.size());
  // Segfault here?
  for (size_t n=0; n<cloud.points_.size(); n++) {
    float d = cloud.points_[n][2];
    float i = calib[1]*cloud.points_[n][1]/d + calib[3];
    float j = calib[0]*cloud.points_[n][0]/d + calib[2];
    if (j >= cols || i >= rows || j < 0 || i < 0)
      continue;
    // Set color
    if (use_color && (
          (depth_dst.at<float>(i, j) < (float)cloud.points_[n][2] * factor) || 
          (depth_dst.at<float>(i, j) == 0))
       )
    {
      color_dst.at<float>(i, j) = cloud.colors_[n][0];
    }
    // Set depth
    if (cloud.points_[n][2] > 0)
      depth_dst.at<float>(i, j) = (float)cloud.points_[n][2] * factor;
  }
  HAIL();
}


void
colorCloudToMatHandleOcclusion(
    cv::Mat & data_counts,
    cv::Mat & depth_dst,
    cv::Mat & color_dst,
    const cv::Mat & query_depth,
    const open3d::geometry::PointCloud & cloud,
    const std::vector<double> & calib,
    const Eigen::Matrix4d trans,
    const int rows, 
    const int cols, 
    const float factor)
{
  // Color is monochrome for efficiency
  float occlusion_margin = 0.08;
  bool use_color = cloud.colors_.size() == cloud.points_.size();
  data_counts = cv::Mat(rows, cols, CV_32FC1, cv::Scalar(0));
  depth_dst = cv::Mat(rows, cols, CV_32FC1, cv::Scalar(0));
  color_dst = cv::Mat(rows, cols, CV_32FC1, cv::Scalar(0));
  for (size_t n=0; n<cloud.points_.size(); n++) {
    Eigen::Vector4d pt (cloud.points_[n][0], cloud.points_[n][1], cloud.points_[n][2], 1);
    pt = trans * pt;
    if (pt[2] <= 0) // point in front of camera
      continue;

    float j = calib[0]*pt[0]/pt[2] + calib[2];
    float i = calib[1]*pt[1]/pt[2] + calib[3];
    if (j >= cols || i >= rows || j < 0 || i < 0)
      continue;

    // Handle occlusion
    if (std::abs(pt[2] - float(query_depth.at<ushort>(i, j)/factor)) > occlusion_margin)
      continue;

    if ((depth_dst.at<float>(i, j) == 0) || 
        (depth_dst.at<float>(i, j) < factor * (pt[2] - occlusion_margin))) {
      // If uninitialized or
      // Check if there is already something closer
      depth_dst.at<float>(i, j) = pt[2] * factor;
      data_counts.at<float>(i, j) = 1;
      if (use_color)
        color_dst.at<float>(i, j) = cloud.colors_[n][0];
    } else {
      // Add to existing value
      depth_dst.at<float>(i, j) = pt[2] * factor;
      data_counts.at<float>(i, j) += 1;
      if (use_color)
        color_dst.at<float>(i, j) += cloud.colors_[n][0];
    }
  }
  HAIL();
}

open3d::geometry::PointCloud 
depthToCloud(
    const cv::Mat & depth,
    const cv::Mat * color,
    const std::vector<double> & calib,
    const float factor,
    const BoundingBox & bb,
    const Eigen::Matrix4d & transform,
    const bool interp_cloud,
    const int decimation_factor,
    const float pixels_per_meter)
{
  open3d::geometry::PointCloud out;
  out.points_.reserve(depth.cols*depth.rows);
  if (color != NULL)
    out.colors_.reserve(depth.cols*depth.rows);
  // for(int j = bb.x; j < std::min(int(bb.x+bb.w), depth.cols); j+=decimation_factor)
  // {
  //   for(int i = bb.y; i < std::min(int(bb.y+bb.h), depth.rows); i+=decimation_factor)
  //   {
  for (int i = 0; i < depth.rows; i++) {
    for (int j = 0; j < depth.cols; j++) {
      // float dep = float(depth.at<ushort>(i, j)) / factor;
      float dep;
      if (factor == 1) {
        dep = depth.at<float>(i, j);
      } else {
        dep = float(depth.at<ushort>(i, j)) / factor;
      }
      if (dep > 0.02f)
      {
        // Calculate how wide the pixel is to interpolate the missing area
        // Round up so there is always at least one pixel

        if (!interp_cloud) {

          Eigen::Vector4d pt;
          pt[0] = float(j - calib[2]) * dep / calib[0];
          pt[1] = float(i - calib[3]) * dep / calib[1];
          pt[2] = dep;
          pt[3] = 1;

          // Eigen::Vector4d pt;
          // pt[1] = float(j - calib[2]) * dep / calib[0];
          // pt[2] = float(i - calib[3]) * dep / calib[1];
          // pt[0] = dep;
          // pt[3] = 1;

          Eigen::Vector4d new_point = transform * pt;
          out.points_.push_back(new_point.head<3>());
          if (color != NULL) {
            Eigen::Vector3d c;
            // c << double(color->at<float>(i, j)), 0, 0;
            c << double(color->at<float>(i, j)) * dep * dep, 0, 0;
            out.colors_.push_back(c);
          }
        } else {
          // Get depth values for surrounding points
          float dep_up = float(depth.at<ushort>(std::max(i-1, 0), j)) / factor;
          float dep_down = float(depth.at<ushort>(std::min(i+1, depth.rows), j)) / factor;
          float dep_right = float(depth.at<ushort>(i, std::min(j+1, depth.cols))) / factor;
          float dep_left = float(depth.at<ushort>(i, std::max(j-1, 0))) / factor;
          dep_up = (dep_up != 0) ? dep_up : dep;
          dep_down = (dep_down != 0) ? dep_down : dep;
          dep_right = (dep_right != 0) ? dep_right : dep;
          dep_left = (dep_left != 0) ? dep_left : dep;

          float x = float(j - calib[2]) * dep / calib[0];
          float y = float(i - calib[3]) * dep / calib[1];

          // Get distance to surrounding points
          float dist_up = sqrt(
              pow(dep_up - dep, 2) +
              pow(y - float(i-1-calib[3])*dep_up/calib[1], 2));
          float dist_down = sqrt(
              pow(dep_down - dep, 2) +
              pow(y - float(i+1-calib[3])*dep_down/calib[1], 2));
          float dist_right = sqrt(
              pow(dep_right - dep, 2) +
              pow(x - float(j+1-calib[2])*dep_right/calib[0], 2));
          float dist_left = sqrt(
              pow(dep_left - dep, 2) +
              pow(x - float(j+1-calib[2])*dep_left/calib[0], 2));

          // Check if point is isolated
          // Distance between pixels grows as depth to pixel grows
          // This erodes edges but it's necessary
          if ((dist_up > 1/dep/2 ||
               dist_down > 1/dep/2) ||
              (dist_right > 1/dep/2 ||
               dist_left > 1/dep/2)) {
            // Point is isolated
            continue;
          }

          // Now calculate how many points are needed to interpolate
          // At 1 meter, 1 meter wide = calib[0]*2 number of pixels
          // Can be reduced to save on CPU time
          int pixel_w = std::max(
              std::min(
                (dist_up+dist_down)*pixels_per_meter,
                20.0f),
              1.0f);
          int pixel_h = std::max(
              std::min(
                (dist_right+dist_left)*pixels_per_meter,
                20.0f),
              1.0f);
          // TODO for loop for each side
          // printf("dist_up: %f\n", dist_up);
          // printf("dist_down: %f\n", dist_down);
          // printf("dist_right: %f\n", dist_right);
          // printf("dist_left: %f\n", dist_left);
          // printf("dep: %f\n", dep);
          // printf("pixel_w: %i\n", pixel_w);
          // printf("pixel_h: %i\n", pixel_h);

          for (int w_sub = 0; w_sub < pixel_w; w_sub++) {
            for (int h_sub = 0; h_sub < pixel_h; h_sub++) {
              // Piecewise depth interpolation
              float sub_pix_x = float(w_sub)/pixel_w;
              float sub_pix_y = float(h_sub)/pixel_h;
              float dep_horz_interp;
              if (w_sub <= dep/calib[0]/2) {
                // Interpolate using left point
                dep_horz_interp = dep*(sub_pix_x*2) + dep_left*(1-sub_pix_x*2);
              } else {
                // Interpolate using right point
                dep_horz_interp = dep*(2-sub_pix_x*2) + dep_right*(sub_pix_x*2 - 1);
              }

              float dep_vert_interp;
              if (h_sub <= dep/calib[1]/2) {
                // Interpolate using up point
                dep_vert_interp = dep*(sub_pix_y*2) + dep_up*(1-sub_pix_y*2);
              } else {
                // Interpolate using down point
                dep_vert_interp = dep*(2-sub_pix_y*2) + dep_down*(sub_pix_y*2 - 1);
              }

              float dep_interp = (dep_vert_interp+dep_horz_interp)/2;
              Eigen::Vector4d pt (
                  float((sub_pix_x+j) - calib[2]) * dep_interp / calib[0],
                  float((sub_pix_y+i) - calib[3]) * dep_interp / calib[1],
                  dep_interp,
                  1);
              Eigen::Vector4d new_point = transform * pt;
              out.points_.push_back(new_point.head<3>()/new_point(3));
              if (color != NULL) {
                Eigen::Vector3d c (double(color->at<float>(i, j)) * dep_interp * dep_interp, 0, 0);
                // c << double(color->at<float>(i, j)), 0, 0;
                out.colors_.push_back(c);
              }
            }
          }
          // END INTERP
        }
      }
      // ELSE NOTHING
    }
  }
  return out;
}

void
denoise_depth(const cv::Mat & depth, cv::Mat & out)
{
  // Make more smooth
  // cv::blur(depth, out, cv::Size(5, 5));
  cv::threshold(depth, out, 1e-10, 1, 0);
  // Remove specs
  // cv::morphologyEx(out, out, cv::MORPH_OPEN, KERNEL7);
  // Close holes
  cv::morphologyEx(out, out, cv::MORPH_CLOSE, KERNEL7);
}
