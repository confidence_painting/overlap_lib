#include <string>
#include <Open3D/Open3D.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/tokenizer.hpp>

#include "overlap_lib/projection.h"
#include "overlap_lib/json.hpp"
#include "overlap_lib/util.h"

#define SCANNET false

using json = nlohmann::json;
using namespace std;


const float factor = (SCANNET) ? 1000 : 512;
const std::string base_path = "/externd/datasets/views3/gibson/Adrian/";
// const std::string base_path = "/externd/datasets/scannet/images/scene0000_00/";
// const std::string base_path = "/data/scannet/images/scene0000_00/";

const std::string calib_path = "calib_depth_nice.txt";

#if SCANNET
  const char * colors = "frame-%06d.color.jpg";
  const char * depths = "frame-%06d.depth.pgm";
  const char * poses = "frame-%06d.pose.txt";
  const int num_images = 40;
  const int framerate= 60;
#else
  const char * colors = "rgb/%07d.png";
  const char * depths = "depth/%07d.tiff";
  const char * poses = "metadata/%07d.json";
  const int num_images = 30;
  // const int num_images = 19;
  const int framerate= 1;
#endif

int main() {
  // Run reconstruction
  open3d::geometry::PointCloud recon;

  // Load calibration
  std::vector<double> calib;
  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > ts;

#if SCANNET
    std::ifstream calibf(base_path + calib_path);
    if (!calibf.good()) {
      cout << "Failed to open calib file: " << base_path+calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(calibf)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    calib = {fx, fy, cx, cy};
    std::cout << calib << std::endl;
    calib = {fx*320/640, fy*240/480, cx*320/640, cy*240/480};
    std::cout << calib << std::endl;
#else
    calib = {256, 256, 256, 256};
#endif
  for (int i=0; i < num_images; i++) {
    char buf[40];
    sprintf(buf, poses, i*framerate);

#if SCANNET
    Eigen::Matrix4d t = loadScannetPose(base_path+string(buf)).cast<double>();
#else
    Eigen::Matrix4d t = loadGibsonPose(base_path+string(buf)).cast<double>();
#endif
    ts.push_back(t);
  }

  for (int i=0; i < num_images; i++) {
    char buf[40];
    sprintf(buf, poses, i*framerate);
    
#if SCANNET
    Eigen::Matrix4d t = loadScannetPose(base_path+string(buf)).cast<double>();
#else
    Eigen::Matrix4d t = loadGibsonPose(base_path+string(buf)).cast<double>();
#endif
    // ts.push_back(t);
    
    sprintf(buf, depths, i*framerate);
    cv::Mat depth_im = cv::imread(base_path+string(buf), -1);
    cv::resize(depth_im, depth_im, cv::Size(320, 240), 0, 0, cv::INTER_NEAREST);
    // sprintf(buf, colors, i*framerate);
    // std::cout << base_path+string(buf) << std::endl;
    // cv::Mat color_im = cv::imread(base_path+string(buf), -1);
    std::cout << depth_im.rows << std::endl;
    std::cout << depth_im.cols << std::endl;

    // Load cloud
    open3d::geometry::PointCloud pc =
    depthToCloud(
        depth_im,
        NULL,
        calib,
        factor, // factor
        BoundingBox(0, 0, depth_im.cols, depth_im.rows),
        t, false);
    std::cout << pc.points_.size() << std::endl;
    recon += pc;

    recon.Transform(ts[20].inverse());
    cv::Mat depth_dst, color_dst;
    colorCloudToMat(depth_dst, color_dst, recon, calib, 320, 240, factor);
    recon.Transform(ts[20]);
    depth_dst.convertTo(depth_dst, CV_16U);

    // cv::imshow("Color_dst", color_dst);
    // cv::imshow("Depth_dst", depth_dst*10);
    // cv::imshow("Depth_im", depth_im*10);
    // cv::waitKey(0);
  }

    std::shared_ptr<const open3d::geometry::PointCloud> pc_ptr (&recon);
    open3d::visualization::DrawGeometries({pc_ptr}, "Backproj", 500, 500, 250, 250);
    return 0;
}
