#include <torch/script.h>
#include <opencv2/highgui/highgui.hpp>
#include "overlap_lib/test_util.h"
#include "overlap_lib/util.h"
#include "overlap_lib/projection.h"
#include "overlap_lib/json.hpp"

using json = nlohmann::json;

void
gen_filter(
    const int & radius,
    const float & bin_offset,
    cv::Mat & filt)
{
  // Generates a circle filter 
  filt = cv::Mat::zeros(2*radius+1, 2*radius+1, CV_16SC1);
  const int center = radius;
  for (int i=0; i<filt.rows; i++) {
    for (int j=0; j<filt.cols; j++) {
      const float dist = sqrt(pow(float(i-center), 2) + pow(float(j-center), 2));
      if (dist > float(radius)-1+bin_offset && dist < float(radius)+bin_offset) {
        filt.at<int16_t>(i, j) = 1;
      }
    }
  }
}

void
nearest_neighbor_search(
    const cv::Mat & query,
    const cv::Mat & cand,
    const std::vector<cv::Mat> & dist_filters,
    cv::Mat & distances)
{
  // Values in query and cand must be 0 or 1 to represent whether a node exists
  // We test distance from nodes in the candidate to those in the query
  // Distances are offset by 1. 0 = no match, 1 = 0, ...
  cv::Mat query_binary, cand_binary;
  cv::threshold(query, query_binary, 0.1, 1, 0);
  cv::threshold(cand, cand_binary, 0.1, 1, 0);

  // First, if a node is on top of another, the distance is 0
  cv::Mat distances_f;
  cand_binary.copyTo(distances_f);
  distances_f.setTo(255, distances_f == 0);

  // Then, for each filter, convolve with the candidates
  cv::Mat output;
  for (size_t i=0; i<dist_filters.size(); i++) {
    // Get distance
    cv::filter2D(cand_binary, output, -1, dist_filters[i]);
    // Set value of pixels that lie within the band to the distance to that band
    output.setTo(i+2, output >= 1);
    output.setTo(255, output == 0);

    // Set distance value in distances to be that value if it's the shortest
    cv::min(distances_f, output, distances_f);
  }
  // This method actually finds the shortest distance to any node on the grid
  // Mask distances values to those where query nodes exist
  cv::Mat mask;
  query_binary.convertTo(mask, CV_8UC1, 255);
  HPRINT(mask.size());
  HPRINT(distances_f.size());
  distances_f.copyTo(distances, mask);
}

torch::Tensor
detection_accuracy_histogram(
    std::vector<torch::Tensor> detection_maps,
    const torch::Tensor & pair_map,
    const std::vector<std::string> & depth_paths,
    const std::vector<std::string> & pose_paths,
    const int64_t max_dist,
    const std::string & type,
    const std::string & calib_path)
{

  /*
   * Creates a histogram with max_dist number of bins that catelogs how
   * consistent the detections are from one frame to the next
   * Last bin is for unmatched detections
   */

  //============================================================================
  //                           Same Loading Code
  //============================================================================

  HAIL();
  // Load pose and depth
  const int batch_size = pose_paths.size();
  const int im_rows = detection_maps[0].size(0);
  const int im_cols = detection_maps[0].size(1);
  HPRINT(im_rows);
  HPRINT(im_cols);
  HPRINT(batch_size);
  bool DISPLAY = false;

  // Load images and poses
  std::vector<cv::Mat> images, score_maps;
  images.reserve(batch_size);
  score_maps.reserve(batch_size);

  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > poses;
  for (int i=0; i<batch_size; i++) {
    // Load stuff
    cv::Mat im = cv::imread(depth_paths[i], -1);
    HPRINT(depth_paths[i]);
    cv::resize(im, im, cv::Size(im_cols, im_rows), 0, 0, cv::INTER_NEAREST);
    images.push_back(im);
    HPRINT(pose_paths[i]);
    if (type == "gibson") {
      poses.push_back(loadGibsonPose(pose_paths[i]).cast<double>());
    } else {
      poses.push_back(loadScannetPose(pose_paths[i]).cast<double>());
    }

    // Convert tensor to cv::Mat
    cv::Mat score_map (im_rows, im_cols, CV_16SC1,
                       detection_maps[i].data<int16_t>());
    HPRINT(detection_maps[i].size(0));
    HPRINT(detection_maps[i].size(1));
    score_maps.push_back(score_map);
  }
  HAIL();

  // Assumes all images are the same size
  const BoundingBox full_bbx(0, 0, im_cols, im_rows);

  std::vector<double> calib;
  float depth_factor = 512;
  if (type == "gibson") {
    calib = {
        CALIB_REF.fx*im_cols, CALIB_REF.fy*im_rows,
        CALIB_REF.cx*im_cols, CALIB_REF.cy*im_rows};
    depth_factor = 512;
  } else {
    HAIL();
    assert(file_exists(calib_path));
    std::ifstream t(calib_path);
    if (!t.good()) {
      cout << "Failed to open calib file: " << calib_path << endl;
      exit(-1);
    }
    std::string str((std::istreambuf_iterator<char>(t)),
                     std::istreambuf_iterator<char>());
    json js = json::parse(str);

    float fx = js["fx"];
    float fy = js["fy"];
    float cx = js["cx"];
    float cy = js["cy"];

    // Calculate the scaling factor for the calibration
    cv::Mat im = cv::imread(depth_paths[0], -1);

    // Someone tell me why this doesn't work.
    // std::vector<double> calib (float(js["fx"]), float(js["fy"]), float(js["cx"]), float(js["cy"]));
    // calib = std::vector<double>(fx, fy, cx - roi.x, cy - roi.y);
    calib = {fx/im.cols*im_cols, fy/im.rows*im_rows, cx/im.cols*im_cols, cy/im.rows*im_rows};
    depth_factor = 1000;
  }

  //============================================================================
  //                                Run test
  //============================================================================

  // Generate the distance filters for performing fast nn search
  // The bin offset could be iterated over to generate a really nice,
  // accurate histogram, but that's expensive
  std::vector<cv::Mat> filters (max_dist-1);
  // Filters do not cover 0 dist
  for (int d=1; d<max_dist; d++) {
    cv::Mat filter;
    gen_filter(d, 0.5f, filter);
    filters[d-1] = filter;
  }
  HAIL();

  // Last bin is for unmatched detections
  torch::Tensor histogram = torch::zeros({max_dist+1}, torch::kInt);
  HAIL();

  // iterate over pairs and run test
#if PARALLEL
// #pragma omp parallel for schedule(static)
#endif

  double total_angle_change = 0;
  for (int i=0; i<batch_size; i++) {
    for (int j=i; j<batch_size; j++) {
      if (i==j)
        continue;
      if (pair_map[i][j].item<int16_t>() == 0) {
        continue;
      }
      HAIL();
      // Now calculate the angles
      Eigen::Vector3d p1 = poses[i].block<3,3>(0,0).eulerAngles(0, 1, 2);
      Eigen::Vector3d p2 = poses[j].block<3,3>(0,0).eulerAngles(0, 1, 2);
      Eigen::Vector3d ea = p1 - p2;
      double angle_change = 0;
      if (i+1 == j) {
        angle_change = (abs(ea[0]) + abs(ea[1]) + abs(ea[2]))/M_PI*180;
      }

      // Backproject detections from candidate into query
      cv::Mat bp_detections, bp_mask;
      cv::Mat float_scores;
      score_maps[j].convertTo(float_scores, CV_32FC1);
      back_project_score_map(
          images[i], images[j],
          poses[i], poses[j],
          calib, calib,
          float_scores,
          depth_factor,
          false,
          bp_detections,
          bp_mask);
      HAIL();
      // Convert depth to mask
      bp_mask.convertTo(bp_mask, CV_8UC1, 255);
      cv::dilate(bp_mask, bp_mask, KERNEL3);
      // Make sure detections are either 0 or 1
      // cv::threshold(bp_detections, bp_detections, 0.1, 1, 0);
      // bp_detections.convertTo(bp_detections, CV_8UC1);
      // cv::Mat nonZeroCoordinates;
      // cv::findNonZero(bp_detections, nonZeroCoordinates);
      // std::cout << nonZeroCoordinates << std::endl;
      bp_detections.convertTo(bp_detections, CV_16UC1);

      HAIL();
      if (DISPLAY) {
        cv::imshow("Backproj det", bp_detections*255);
        cv::imshow("Query", score_maps[i]*255);
        cv::waitKey(0);
      }

      // Now, let's compare the backprojected detections to the query detections
      cv::Mat distance_matrix, distance_matrix_raw;
      nearest_neighbor_search(score_maps[i], bp_detections, filters, distance_matrix_raw);
      // Only compare distances with points that exist in the query
      HAIL();
      distance_matrix_raw.copyTo(distance_matrix, bp_mask);
      if (DISPLAY) {
        cv::imshow("DMR", distance_matrix_raw*255);
        cv::imshow("DM", distance_matrix*255);
        cv::imshow("Mask", bp_mask);
        cv::waitKey(0);
      }
      HAIL();
      // Add distances to histogram
#pragma omp critical (assign_gt)
      {
        total_angle_change += angle_change;
        for (int d=0; d<max_dist; d++) {
          // == returns 0 or 255 in opencv
          // Distance matrix is offset by 1. 0 repr unused points
          histogram[d] += int(cv::sum(distance_matrix == d+1)[0])/255;
        }
        // These are the unmatched points
        histogram[max_dist] += int(cv::sum(distance_matrix >= max_dist)[0])/255;
      }
      HAIL();
    }
  }
  std::cout << "Average angle change: " <<
    total_angle_change / ((batch_size * batch_size) / 2 - batch_size) << std::endl;
  return histogram;
}
