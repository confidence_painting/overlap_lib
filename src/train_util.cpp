#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry>

// #include <octomap/octomap.h>
// #include <octomap/OcTree.h>
// #include <octomap/ColorOcTree.h>

#include <torch/script.h>

#include "overlap_lib/util.h"
#include "overlap_lib/train_util.h"
#include "overlap_lib/projection.h"
#include "octree/octree.h"
#include "half.h"

glm::vec3
calcRayPos(
    float i, float j,
    Eigen::Matrix4d & pose,
    const std::vector<double> & calib)
{
    // Ray trace value for each pixel
    Eigen::Vector4d pt(float(j - calib[2]) / calib[0],
                       float(i - calib[3]) / calib[1],
                       1, 1);
    pt = pose * pt;
    glm::vec3 opt = glm::vec3(float(pt[0]), float(pt[1]), float(pt[2]));
    return opt;
}


std::vector<torch::Tensor>
reproject_score_maps(
    std::vector<torch::Tensor> score_maps,
    const std::vector<torch::Tensor> pose_tensors,
    const std::string & octomap_path,
    const std::vector<double> & calib)
{

  /*!
   * A variation of the SuperPoint algorithm applied to bounding boxes
   *
   * pose_paths: list of paths to poses for each image in the batch
   * calib: [fx, fy, cx, cy]
   **/

  // ----------------------------- LOADING ---------------------------------- //

  const int batch_size = score_maps.size();
  const int im_rows = score_maps[0].size(0);
  const int im_cols = score_maps[0].size(1);

  Eigen::AngleAxisd rot (-M_PI/2, Eigen::Vector3d::UnitX());
  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > poses;
  for (int i=0; i<batch_size; i++) {
    torch::Tensor t = pose_tensors[i];
    Eigen::Matrix4d pose;
    pose << t[0][0].item<double>(), t[0][1].item<double>(), t[0][2].item<double>(), t[0][3].item<double>(), 
            t[1][0].item<double>(), t[1][1].item<double>(), t[1][2].item<double>(), t[1][3].item<double>(), 
            t[2][0].item<double>(), t[2][1].item<double>(), t[2][2].item<double>(), t[2][3].item<double>(), 
            t[3][0].item<double>(), t[3][1].item<double>(), t[3][2].item<double>(), t[3][3].item<double>();
    poses.push_back(pose);
  }

  // -------------------------- DONE LOADING -------------------------------- //

  // Compile a map using all viewpoints

  // octomap::ColorOcTree tree (res);
  // tree.readBinary(octomap_path);
  octree::Octree tree (octomap_path);

  std::cout << "Starting to paint map" << std::endl;;

  // We need to apply a rotation to deal with the fact
  // that the mesh is supposed to be z-up

#if PARALLEL
#pragma omp parallel for schedule(static)
#endif
  for(int query_index=0; query_index<batch_size; query_index++) {

    // PROGRESS BAR BEGIN
#pragma omp critical (progress)
    {
      int bar_width = 70;
      std::cout << "[";
      float progress = float(query_index)/batch_size;
      int pos = bar_width*progress;
      for (int i=0; i<bar_width; i++) {
        if (i<pos) std::cout << "=";
        else if (i==pos) std::cout << ">";
        else std::cout << " ";
      }
      std::cout << "] " << int(progress*100.0) << " %\r";
      std::cout.flush();
      // PROGRESS BAR END
    }

    Eigen::Vector4d origin_e(poses[query_index](0, 3),
                             poses[query_index](1, 3),
                             poses[query_index](2, 3), 1);
    glm::vec3 origin = {
        poses[query_index](0, 3),
        poses[query_index](1, 3),
        poses[query_index](2, 3),
    };
    // const octomap::point3d origin (origin_e(0), origin_e(1), origin_e(2));
    for (int i=0; i < im_rows; i++) {
      for (int j=0; j < im_cols; j++) {
        // If there is no data at the pixel, skip
        float score = score_maps[query_index][i][j].item<float>();
        if (score == -1) {
          // These scores have been masked
          continue;
        }

        glm::vec3 opt = calcRayPos(i, j, poses[query_index], calib);

        // Retrieve the endpoint node
        float dist;
        int64_t * res = tree.raycast(origin, opt-origin, &dist);
        float * float_res;
        if (res == NULL) {
          continue;
        }

        float voxels_per_pixel = dist / tree.scale / im_rows;
        voxels_per_pixel = 1;
        if (voxels_per_pixel > 1) {
          for (float sub_i=0; sub_i<1; sub_i+=voxels_per_pixel) {
            for (float sub_j=0; sub_j<1; sub_j+=voxels_per_pixel) {
              opt = calcRayPos(i+sub_i, j+sub_j, poses[query_index], calib);
              res = tree.raycast(origin, opt-origin, &dist);
#pragma omp critical (progress)
              if (res != NULL) {
                float_res = (float *)res;
                float_res[0] += score;
                float_res[1] += 1.0f;
              }
            }
          }
        } else {
#pragma omp critical (progress)
          {
            float_res = (float *)res;
            float_res[0] += score;
            float_res[1] += 1.0f;
          }
        }
      }
    }
  }
  printf("\n");
  
  // tree.write("/home/amai/Downloads/octree.ot");
  // std::string path = "./octree.oct";
  // std::cout << "Saving to: " << path << std::endl;
  // tree.save(path, 2e9);
  std::cout << "Made tree" << std::endl;
  HAIL();

  // ------------------------------ MATCHING -------------------------------- //

  // The format of this is a tensor for each query image, with num x [x y w h]
  // Stores the pos counts x batch_size, then the total_counts x batch_size
  std::vector<torch::Tensor> targets(2*batch_size);
#if PARALLEL
#pragma omp parallel for schedule(static)
#endif
  for(int query_index=0; query_index<batch_size; query_index++) {
    // PROGRESS BAR BEGIN
    /*
    int bar_width = 70;
    std::cout << "[";
    float progress = float(query_index)/batch_size;
    int pos = bar_width*progress;
    for (int i=0; i<bar_width; i++) {
      if (i<pos) std::cout << "=";
      else if (i==pos) std::cout << ">";
      else std::cout << " ";
    }
    std::cout << "] " << int(progress*100.0) << " %\r";
    std::cout.flush();
    */
    // PROGRESS BAR END

    bool DISPLAY = false;

    HAIL();
    cv::Mat pos_counts = cv::Mat::zeros(im_rows, im_cols, CV_32FC1);
    cv::Mat total_counts = cv::Mat::zeros(im_rows, im_cols, CV_32FC1);

    glm::vec3 origin = {
        poses[query_index](0, 3),
        poses[query_index](1, 3),
        poses[query_index](2, 3),
    };
    // Convert transform matrix to euler angles
    for (int i=0; i < im_rows; i++) {
      for (int j=0; j < im_cols; j++) {
        glm::vec3 opt = calcRayPos(i, j, poses[query_index], calib);

        // Retrieve the endpoint node
        float dist;
        int64_t * res = tree.raycast(origin, opt-origin, &dist);
        float * float_res;

        if (res == NULL) {
          continue;
        }

        float voxels_per_pixel = dist / tree.scale / im_rows;
        voxels_per_pixel = 1;
        if (voxels_per_pixel > 1) {
          for (float sub_i=0; sub_i<1; sub_i+=voxels_per_pixel) {
            for (float sub_j=0; sub_j<1; sub_j+=voxels_per_pixel) {
              opt = calcRayPos(i+sub_i, j+sub_j, poses[query_index], calib);
              res = tree.raycast(origin, opt-origin, &dist);
#pragma omp critical (progress)
              if (res != NULL) {
                float_res = (float *)res;
                pos_counts.at<float>(i, j) += float_res[0];
                total_counts.at<float>(i, j) += float_res[1];
              }
            }
          }
        } else {
#pragma omp critical (progress)
          {
            float_res = (float *)res;
            pos_counts.at<float>(i, j) += float_res[0];
            total_counts.at<float>(i, j) += float_res[1];
          }
        }
      }
    }
    HAIL();
    // cv::dilate(total_counts, total_counts, KERNEL5);
    if (DISPLAY) {
      // cv::imshow("Depth", depth/2548);
      // cv::imshow("Data_counts", total_counts);
      cv::imshow("pos_counts", pos_counts);
      cv::waitKey(0);
    }

    HAIL();
    // Convert mask to tensor for transfer
    torch::Tensor pos_counts_t = torch::from_blob(
        pos_counts.ptr<float>(),
        /*sizes=*/{pos_counts.rows, pos_counts.cols}).clone();
    torch::Tensor total_counts_t = torch::from_blob(
        total_counts.ptr<float>(),
        /*sizes=*/{total_counts.rows, total_counts.cols}).clone();
    targets[query_index] = pos_counts_t;
    targets[batch_size + query_index] = total_counts_t;
  }
  HAIL();

  return targets;
}
