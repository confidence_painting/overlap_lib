#include "overlap_lib/types.h"

void BoundingBox::clipWithin(int mx, int my, int mw, int mh){
  int p2x = x+w;
  int p2y = y+h;

  x = std::min(std::max(x, 0), mw);
  y = std::min(std::max(y, 0), mh);
  w = std::max(std::min(p2x, mw) - x, 0);
  h = std::max(std::min(p2y, mh) - y, 0);
}

std::ostream& operator<<(std::ostream& os, const BoundingBox b) {
  return os << "x: " << b.x << std::endl <<
               "y: " << b.y << std::endl <<
               "w: " << b.w << std::endl <<
               "h: " << b.h << std::endl;
}

std::ostream& operator<<(std::ostream& os, const Calibration c) {
  return os << "fx: " << c.fx << std::endl <<
               "fy: " << c.fy << std::endl <<
               "cx: " << c.cx << std::endl <<
               "cy: " << c.cy << std::endl;
}
