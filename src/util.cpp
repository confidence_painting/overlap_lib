#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <Eigen/Geometry>

#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>

#include <boost/tokenizer.hpp>

#include "overlap_lib/util.h"
#include "overlap_lib/json.hpp"

// Backtrace
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;
using json = nlohmann::json;

std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "CUSTOM"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

Eigen::Matrix4f
loadUnrealCVPose(const std::string & poseFilePath) {
  std::ifstream t(poseFilePath);
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());
  json js = json::parse(str);
  // Eigen::Translation3f translation (float(js["position"][0]), float(js["position"][1]), float(js["position"][2]));
  float s = 0.01; // cm to m
  // Eigen::Translation3f translation ((js["position"][0]), (js["position"][1]), (js["position"][2]));
  Eigen::Translation3f translation (s*float(js["position"][0]), s*float(js["position"][1]), -s*float(js["position"][2]));
  //Eigen::Translation3f translation (0,0,0);
  // Translate, then rotate
  // Eigen::Quaternionf q =
  //   Eigen::AngleAxisf(float(js["pitchyawroll"][1])/180*M_PI, Eigen::Vector3f::UnitZ())
  //   * Eigen::AngleAxisf(float(js["pitchyawroll"][0])/180*M_PI, Eigen::Vector3f::UnitY())
  //   * Eigen::AngleAxisf(float(js["pitchyawroll"][2])/180*M_PI, Eigen::Vector3f::UnitX());
  // std::cout << float(js["pitchyawroll"][1]) << std::endl;
  Eigen::Quaternionf q =
    Eigen::AngleAxisf((float(js["pitchyawroll"][1]))/180*M_PI, Eigen::Vector3f::UnitZ())
    * Eigen::AngleAxisf(float(js["pitchyawroll"][0])/180*M_PI, Eigen::Vector3f::UnitY());

  // original: z up, x forward, y right
  // ours: z forward, y up, x right
  // This correction takes ours -> original
  Eigen::Quaternionf corr = 
      //Eigen::AngleAxisf(-M_PI/2, Eigen::Vector3f::UnitY()) *
      Eigen::AngleAxisf(M_PI/2, Eigen::Vector3f::UnitY()) * 
      Eigen::AngleAxisf(M_PI/2, Eigen::Vector3f::UnitX()) *
      Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY());

  // return (corr.inverse() * (translation * q) * corr).matrix();
  // return ((translation * q) * corr).matrix();
  return ((translation * q)).matrix();
  // return ((translation * q) * corr).matrix();
}

Eigen::Matrix4f
loadGibsonPose(const std::string & poseFilePath) {
  std::ifstream t(poseFilePath);
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  std::string str((std::istreambuf_iterator<char>(t)),
                   std::istreambuf_iterator<char>());
  json js = json::parse(str);
  float s = 1;
  Eigen::Translation3f translation (s*float(js["position"][0]), s*float(js["position"][1]), s*float(js["position"][2]));
  //Eigen::Translation3f translation (s*float(js["position"][0]), s*float(js["position"][1]), float(0));
  //Eigen::Translation3f translation (float(0), s*float(js["position"][1]), s*float(js["position"][2]));
  //Eigen::Translation3f translation (0,0,0);
  Eigen::Quaternionf q(js["orientation"][3],
                       js["orientation"][0],
                       js["orientation"][1],
                       js["orientation"][2]);
  // Translate, then rotate
  //coc = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());
  //coc = Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitX());

  // Input transform: x front, y left, z up
  // Input cloud: z front, x right, y up
  //coc = 
      //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());
  // Translate to ROS Coords
  Eigen::Quaternionf corr;
  corr = 
      //Eigen::AngleAxisf(-M_PI/2, Eigen::Vector3f::UnitY()) *
      Eigen::AngleAxisf(-M_PI/2, Eigen::Vector3f::UnitX()) * 
      Eigen::AngleAxisf(M_PI/2, Eigen::Vector3f::UnitY()) *
      Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY());
  //corr = Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());

  // Apply the correction from PyBullet -> ROS
  //Eigen::Quaternionf corr;
  //corr = Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitZ())
    //* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitY())
    //* Eigen::AngleAxisf(-M_PI_2, Eigen::Vector3f::UnitX());

  return (translation * q * corr).matrix();
}

Eigen::Matrix4f
loadScannetPose(const std::string & poseFilePath) {
  std::ifstream t(poseFilePath);
  if (!t.good()) {
    cout << "Failed to open pose file: " << poseFilePath << endl;
    exit(-1);
  }
  Eigen::Matrix4f out;
  
  std::vector <float> temp;
  //temp.reserve(4*4);
  for (std::string line; getline(t, line); ) {
    boost::tokenizer<boost::escaped_list_separator<char> > tk(
       line, boost::escaped_list_separator<char>('\\', ' ', '\"'));

    for (boost::tokenizer<boost::escaped_list_separator<char> >::iterator i(tk.begin());
       i!=tk.end();++i) 
    {
       temp.push_back(atof(i->c_str()));
    }
  }
  out << temp[0],  temp[1],  temp[2],  temp[3], 
         temp[4],  temp[5],  temp[6],  temp[7], 
         temp[8],  temp[9],  temp[10], temp[11], 
         temp[12], temp[13], temp[14], temp[15];
  return out;
}

void
stat_mat(const cv::Mat mat, const char * name)
{
  printf("%s: (%i, %i, %i)\n", name, mat.rows, mat.cols, mat.channels());
}

/*
BoundingBox
convertBox(
    torch::Tensor bbx,
    int rows,
    int cols)
{
    // Convert candidate box to image coordinates
    float x = bbx[0].item<float>()*cols;
    float y = bbx[1].item<float>()*rows;
    float w = bbx[2].item<float>()*cols;
    float h = bbx[3].item<float>()*rows;

    BoundingBox cand_box (
          int(x - w/2),
          int(y - h/2),
          (int)ceil(w),
          (int)ceil(h));
    cand_box.clipWithin(0, 0, cols-1, rows-1);

    return cand_box;
}

torch::Tensor
bbx_iou(torch::Tensor boxes1,
        torch::Tensor boxes2,
        bool x1y1x2y2)
{
  torch::Tensor boxes1_x1, boxes1_y1, boxes1_x2, boxes1_y2,
                boxes2_x1, boxes2_y1, boxes2_x2, boxes2_y2;
  if (x1y1x2y2) {
    boxes1_x1 = boxes1.narrow(-1, 0, 1);
    boxes1_y1 = boxes1.narrow(-1, 1, 1);
    boxes1_x2 = boxes1.narrow(-1, 2, 1);
    boxes1_y2 = boxes1.narrow(-1, 3, 1);

    boxes2_x1 = boxes2.narrow(-1, 0, 1);
    boxes2_y1 = boxes2.narrow(-1, 1, 1);
    boxes2_x2 = boxes2.narrow(-1, 2, 1);
    boxes2_y2 = boxes2.narrow(-1, 3, 1);
  } else {
    boxes1_x1 = boxes1.narrow(-1, 0, 1) - boxes1.narrow(-1, 2, 1)/2;
    boxes1_y1 = boxes1.narrow(-1, 1, 1) - boxes1.narrow(-1, 3, 1)/2;
    boxes1_x2 = boxes1.narrow(-1, 0, 1) + boxes1.narrow(-1, 2, 1)/2;
    boxes1_y2 = boxes1.narrow(-1, 1, 1) + boxes1.narrow(-1, 3, 1)/2;

    boxes2_x1 = boxes2.narrow(-1, 0, 1) - boxes2.narrow(-1, 2, 1)/2;
    boxes2_y1 = boxes2.narrow(-1, 1, 1) - boxes2.narrow(-1, 3, 1)/2;
    boxes2_x2 = boxes2.narrow(-1, 0, 1) + boxes2.narrow(-1, 2, 1)/2;
    boxes2_y2 = boxes2.narrow(-1, 1, 1) + boxes2.narrow(-1, 3, 1)/2;
  }

  torch::Tensor boxes1_area = (boxes1_x2-boxes1_x1)*(boxes1_y2-boxes1_y1);
  torch::Tensor boxes2_area = (boxes2_x2-boxes2_x1)*(boxes2_y2-boxes2_y1);

  torch::Tensor lx1 = torch::max(boxes1_x1, boxes2_x1);
  torch::Tensor ly1 = torch::max(boxes1_y1, boxes2_y1);
  torch::Tensor lx2 = torch::min(boxes1_x2, boxes2_x2);
  torch::Tensor ly2 = torch::min(boxes1_y2, boxes2_y2);

  torch::Tensor inter_area = at::clamp(lx2 - lx1, 0) * at::clamp(ly2 - ly1, 0);

  torch::Tensor iou = inter_area / (boxes1_area + boxes2_area - inter_area + 1e-16);
  return iou;
}
*/

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}
